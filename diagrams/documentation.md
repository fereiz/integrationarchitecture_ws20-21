# Documentation
This is the written documentation of the Integration Architecture by Felix Reiz and Florian Demuth.
The source code along with high resolution SVG versions of all graphics can be found here:
https://bitbucket.org/fereiz/integrationarchitecture_ws20-21/src/master/
## Table of Contents
- [Documentation](#documentation)
  - [Table of Contents](#table-of-contents)
  - [1.1. The Old Process](#11-the-old-process)
    - [1.1.1. Description](#111-description)
    - [1.1.2. Context View](#112-context-view)
  - [1.2. Third Party Systems](#12-third-party-systems)
    - [1.2.1. Description](#121-description)
      - [1.2.1.1. OrangeHRM](#1211-orangehrm)
      - [1.2.1.2. OpenCRX](#1212-opencrx)
    - [1.2.2. Business Objects Class Diagrams](#122-business-objects-class-diagrams)
    - [1.2.3. Security Sequence Diagrams](#123-security-sequence-diagrams)
      - [1.2.3.1. OrangeHRM](#1231-orangehrm)
      - [1.2.3.2. OpenCRX](#1232-opencrx)
  - [1.3. High-Performance Application](#13-high-performance-application)
    - [1.3.1. Context View High-Performance](#131-context-view-high-performance)
    - [1.3.2. Package Diagram High-Performance](#132-package-diagram-high-performance)
    - [1.3.3. Physical Distribution High-Performance](#133-physical-distribution-high-performance)
    - [1.3.4. Functional Requirements](#134-functional-requirements)
    - [1.3.5. UI Mock-Up](#135-ui-mock-up)
    - [1.3.6. Code Walkthrough](#136-code-walkthrough)
      - [1.3.6.1. High Performance API](#1361-high-performance-api)
      - [1.3.6.2. High Performance UI](#1362-high-performance-ui)
  - [1.4. Lessons Learned](#14-lessons-learned)
    - [1.4.1. Limitations](#141-limitations)
    - [1.4.2. Future Work](#142-future-work)
## 1.1. The Old Process
### 1.1.1. Description
The general idea is that senior salesmen are paid in two parts:
* a fixed salary
* a variable bonus which depends on:
  * the amount of items sold to highly ranked customers
  * performance ratings from colleagues

Currently the process involves multiple people (CEO, DB Admin, HR), is slow and prone to human errors. Major parts such as the database are not documented and dependent on the DB Admin who does not fully support it.
### 1.1.2. Context View
![](./1_1b_context_view_old_process.drawio.svg)

## 1.2. Third Party Systems

### 1.2.1. Description

#### 1.2.1.1. OrangeHRM
OrangeHRM is an open source HR management tool. In our application it is used to get a list of all employees with their metadata and to update the calculated bonus salaries. It provides a REST API which is documented on the official [Github page](https://orangehrm.github.io/orangehrm-api-doc/). For our purposes we only use the `/employee/search` and `/employee/:id/bonussalary` endpoints. Inside our application the OrangeHRM system is accessed via the repository pattern. This allows us to easily exchange it for any other employee data storage solution. Security is handled via oAuth tokens. The sequence diagram can be found [here](#security-sequence-diagrams).

#### 1.2.1.2. OpenCRX
OpenCRX is a Customer Relationship Management solution that documents the customer interactions and specifically the sales positions.
The High Performance application fetches the necessary sales position data though the API for a given salesman to compute the bonus. The available requests for the API can be explored in a Swagger documentation accessed through the OpenCRX GUI. 
The relevant pieces of information for the bonus calculation of a sale are: The name of the sold product, the client it was sold to, the ranking of the client and the number of items sold.

### 1.2.2. Business Objects Class Diagrams
This is a reduced version of the data returned by orangeHRM:

![](./orange_hrm_classes.drawio.svg)

This is a reduced version of the data returned by OpenCRX:

![](./open_crx_class_diagram.drawio.svg)

### 1.2.3. Security Sequence Diagrams

#### 1.2.3.1. OrangeHRM
![](./2.2b.png)

#### 1.2.3.2. OpenCRX
![](./2.2c.png)

## 1.3. High-Performance Application

### 1.3.1. Context View High-Performance
![](./3_3_context_view_high_performance.drawio.svg)

### 1.3.2. Package Diagram High-Performance 
![](module_view_high_performance_api.drawio.svg)

### 1.3.3. Physical Distribution High-Performance 
![](./3_3_distribution_view_high_performance.drawio.svg)

### 1.3.4. Functional Requirements
The following use-case diagram shows the functional requirements which have been implemented in our new system:  
![](4_1b_use_cases.drawio.svg)

### 1.3.5. UI Mock-Up
This is our original UI mock-up which we developed at the start of the project.  
Apart from styling there are no differences between our UI and the mock-up. While designing we decided to stay as close to the physical bonus computation sheet as possible to allow users of the old process to adapt more quickly to the new system.
![](Wireframe_V1.PNG)

### 1.3.6. Code Walkthrough

#### 1.3.6.1. High Performance API

The High Performance API is a nodejs project which follows the Clean Architecture guidelines. Business logic has been separated from implementation concerns and the use of the SOLID principles ensures that no abstract concepts depend on concrete implementations, making our system highly adaptable and testable. The use of the factory design pattern allows us to remove direct dependencies from our business entities and therefore making them more resilient towards unwanted change.
Core of the project is our business entity, the __record__.

```javascript
export default function buildMakeRecord ({ Id, sanitize, md5, calculateTotalBonus }) {
  return function makeRecord ({
    employeeId,
    id = Id.makeId(),
    // etc...
  } = {}) {
    if (!employeeId) {
      throw new Error('Record must have an employeeId')
    }
    // etc...

    const totalBonus = calculateTotalBonus(bonusA, bonusB)
    let hash

    return Object.freeze({
      getEmployeeId: () => employeeId,
      getId: () => id,
      getDepartment: () => department,
      getYear: () => year,
      getModifiedOnDate: () => modifiedOnDate,
      getRemarks: () => sanitizedRemarks,
      getHash: () => hash || (hash = makeHash()),
      // etc...
    })

    function makeHash () {
      return md5(
        employeeId +
        sanitizedRemarks +
        year +
        department
      )
    }
    // etc...
  }
}

```


__Business rules__

The business rules of our system are kept in the use-case directory. To keep them testable and protect them from unwanted change, they only depend on the business entity. Anything else is injected through the use of the factory pattern.

```javascript
export default function makeListRecords ({ recordsDb }) {
  return async function listRecords ({ employeeId }) {
    let records
    if (employeeId) {
      records = await recordsDb.findByEmployeeId({ employeeId })
    } else {
      records = await recordsDb.findAll()
    }
    return records
  }
}
```

__Controllers__

Since we do not want our code to depend on external frameworks such as express, the use-cases are implemented by controller objects, which map httprequests to their corresponding use-case. The controllers are than handed over to an express instance which handles http requests to our predefined routes.

```javascript
export default function makeGetEmployees ({ listEmployees }) {
  return async function getEmployees (httpRequest) {
    const headers = {
      'Content-Type': 'application/json'
    }
    try {
      let postEmployees
      if (httpRequest.query) {
        postEmployees = await listEmployees(httpRequest.query)
      } else {
        postEmployees = await listEmployees()
      }
      return {
        headers,
        statusCode: 200,
        body: postEmployees
      }
    } catch (e) {
      return {
        headers,
        statusCode: 400,
        body: {
          error: e.message
        }
      }
    }
  }
}
```

__Data access__

The databases are abstracted using the repository design pattern. This allows us to stay flexible in regards to the choice of underlying database. To the system the access to the mongoDB behaves the same as the access to the external REST APIs.

__MongoDB access__
```javascript
import Id from '../Id'

export default function makeRecordsDb ({ makeDb }) {
  return Object.freeze({
    insert,
    findAll,
    findById,
    findByHash,
    findByEmployeeId,
    update,
    remove
  })

  async function findAll () {
    const db = await makeDb()
    const result = await db.collection('records').find({})
    return (await result.toArray()).map(({ _id: id, ...found }) => ({
      id,
      ...found
    }))
  }

  async function findByHash (record) {
    const db = await makeDb()
    const result = await db.collection('records').find({ hash: record.hash })
    const found = await result.toArray()
    if (found.length === 0) {
      return null
    }
    const { _id: id, ...foundInfo } = found[0]
    return { id, ...foundInfo }
  }

  // etc ...
}
```

__OrangeHRM access__
```javascript
export default function makeEmployeeDb ({ makeHRM }) {
  return Object.freeze({
    findAll,
    findById,
    updateBonusSalary
  })

  async function findAll () {
    const hrm = await makeHRM()
    const result = await hrm.get('/api/v1/employee/search')
    return result.data.data
  }

  async function findById ({ id: _id }) {
    const hrm = await makeHRM()
    const result = await hrm.get(`/api/v1/employee/${_id}`)
    return result.data.data
  }

  async function updateBonusSalary ({ employeeId, year, value }) {
    const hrm = await makeHRM()
    const result = await hrm.post(`/api/v1/employee/${employeeId}/bonussalary?year=${year}&value=${value}`)
    return result.data
  }
}
```

#### 1.3.6.2. High Performance UI

The frontend of the Application is an Angular Project consisting mainly of several (nested) components, which handle the look and functionality of the different parts of the website and services, which serve as the data providers that communicate with the backend API.

<b>home.component.html</b>
```HTML
<div id="content" class="border border-primary">
  <div class="border border-primary" id="employeeList" *ngIf="!isSalesman">
    <h2 class="text-info">Employees</h2>
    <app-employee></app-employee>
  </div>

  <div class="border border-primary" id="recordList" >
    <h2 class="text-info mb-0 pb-0">Records</h2>
    <br>
    <app-record></app-record>
  </div>

  <div class="border border-primary" id="recordDetailView">
    <app-details></app-details>
  </div>
</div>
```
The HTML of the home component which incorporates several other components such as the employee component with the \<app-employee> tag.
As an example we will take a closer look at that employee-component in the next section.
<br>

<b>employee.component.ts</b>

```typescript
// Imports...

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})

export class EmployeeComponent implements OnInit {

  employees!: Employee[];
  employeesAll!: Employee[];
  loading: boolean = false;

  constructor(private employeeService: EmployeeService, private recordService: RecordService) {
  }

  ngOnInit(): void {
    this.getEmployees();
  }

  getEmployees(): void {
    this.loading = true;
    this.employeeService.getEmployees()
      .subscribe(employees => {
        employees.sort((a: Employee, b: Employee) => {
          return a.fullName > b.fullName ? 1 : -1;
        });
        this.employeesAll = employees;
        this.employees = this.employeesAll;
        this.loading = false;
      });
  }

  onSelect(employee: Employee): void {
    this.employeeService.setSelectedEmployee(employee);
    this.recordService.setSelectedRecord(null);
  }

// Additional methods...
}
```

<b>employee.service.ts</b>

```typescript
// Imports...

@Injectable({
  providedIn: 'root'
})
export default class EmployeeService {

  constructor(private http: HttpClient) { }
  
  private employeeUrl = `${BACK_END_API_URL}/employees`

  private selectedEmployee!: Employee;

  getSelectedEmployee(): Employee {
    return this.selectedEmployee;
  }

  setSelectedEmployee(employee: Employee) {
    this.selectedEmployee = employee;
    this.employeeSelected.emit(this.selectedEmployee);
  }

  getEmployees(): Observable<Employee[]> {
    return this.http.get<Employee[]>(this.employeeUrl);
  }

  getEmployeeById(id: number | string): Observable<Employee[]> {
    return this.http.get<Employee[]>(this.employeeUrl, {
      params: new HttpParams().set("code", String(id))
    });
  }

  @Output() public employeeSelected: EventEmitter<Employee> = new EventEmitter();
}

```

<b>employee.component.html</b>

```html
<!-- Additional html elements... -->

<ul class="list-group mb-1 itemList">
    <li class="btn btn-primary listItem" *ngFor="let employee of employees" [ngClass]="{'active': isSelected(employee)}"
        (click)="onSelect(employee)">
        {{employee.fullName}}
    </li>
</ul>
```

When the component is created it also creates an instance of the EmployeeService which it then immediately uses on initialization to fetch the employees.

They are fetched from the backend API via the HttpClient class which returns an observable the employee-component subscribes to. As soon as the request is fulfilled, the received employees are sorted and stored in a local property of the component.

In the HTML part of the component there is already an unordered list defined that will display the employees. The *ngFor directive takes the array of employees and renders a list item for every employee element. Due to the reactive nature of Angular, this list will automatically re-render as soon as changes are detected in the "employee" property. That is why there won't be a list displayed immediately after loading the page but as soon as the employees are fetched they are shown.

When clicking a list item another method of the EmployeeService gets called again and sets that employee object as the currently selected employee for further usage. Additionally, an event is emitted that notifies every subscriber of that event emitter, mainly the record component. The record component then fetches and displays the records for the selected employee in a similar fashion.



## 1.4. Lessons Learned

In the Angular project there are several violations against the DRY principle. A lot of the components do similar things. With better and earlier planning we could have created more reusable components that take different arguments instead.

It is also important not to lose yourself in extra features that aren't part of the requirements because it will always take more time than expected and in the end required features get left out.

Our first iteration of the backend can be found in the `deprecated` directory. For this we simply followed online tutorials on how to put together different frameworks and _just make things work_. Even though this got us up and running pretty quickly, the resulting code was a mess. Small changes would sometimes affect seemingly unrelated parts of our project. Writing unit tests was a nightmare since we always had to start out by mocking many dependencies. Searching for ways to make writing tests easier we found Robert Martin's Clean Code and Clean Architecture guidelines.

Trying our best to adhere to clean code principles and building on top of the clean architecture model was tedious in the beginning. Test first TDD was slow in the beginning. It is easy to cheat the principles and requires discipline to stay the course. But in the end the effort paid off greatly. We were able to quickly fix bugs and confidently change parts of our systems since the testcoverage told us if we would break something else.

### 1.4.1. Limitations

The current prototype still has several security flaws. The enforced roles in the frontend can in part be bypassed through manual REST requests (e.g. an employee could fetch and look at every record of their colleagues).

There also isn't much error handling in the frontend as of yet. If things would go wrong, the user sometimes wouldn't even know about it unless he looked at the browser console.

### 1.4.2. Future Work

Aside from making the application safer and more secure, there are still a variety of features that could be implemented, for example:

- An admin dashboard for user management
- A dashboard where the CEO can modify the bonus logic over a GUI. A salesman user could then see (but not modify) the logic in detail.
- A management dashboard with visualized aggregated values like it is described in the nice-to-have section of the requirements.
- More styling and usability features (e.g. responsive design for mobile devices, data caching to prevent lost changes)