# Object Creation in JavaScript #


## Overview ##
In JavaScript, objects are variables that can contain multiple values.
You can create an object by supplying key-value pairs. The "keys" of an object are called properties.

```
var myCar = {brand: "BMW", color: "blue", horsepower: "300"}
```

You can read the property of an object:

```
var myBrand = myCar.brand;
```

You can modify it:

```
myCar.brand = "Audi";
```

You can even add new properties to existing objects:

```
myCar.mileage = 10000;
```
## Methods ##

An Object can also have functions stored as properties:

```
var myCar = {
  brand: "BMW",
  color: "blue",
  horsepower: "300"
  mileage: 10000,
  drive: function(miles){
    this.mileage += miles;
  } 
}

myCar.drive(10);
```

## Prototyping ##
JavaScript traditionally didn't have classes from which to create objects.
Instead we can use a prototype object from which to derive other objects. This way we can also inherit properties. 
To create a constructor we declare a function, which we can then invoke with the keyword <em>new</em> to create a new object that inherits its parent's properties.

```
function Car(brand, color, horsepower, mileage){
  this.brand = brand;
  this.color = color;
  this.horsepower = horsepower;
  this.mileage = mileage;
  this.drive: function(miles){
    this.mileage += miles;
  } 
}

var myCar = new Car("BMW", "Blue", 10000);
myCar.drive(10);
```

### References ###
https://www.w3schools.com/js/
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object