# Functions and Call Back Handler #

## Functions in JavaScript ##
Functions in JavaScript can be defined using the <em>function</em> keyword and invoked at any time.

```
function add(a, b){
    return a + b;
}

var result = add(1,2);
```
It is also possible to store a function in a variable and invoke it using said variable.

```
var f = (a, b) => {return a + b}

var result = f(1,2);
```
The arrow function expression used here is a way to simplify code for simple functions.

## Hoisting ##
In JavaScript, all declared functions in a scope are read before execution. This way you can invoke a in the code before it is declared. This is called hoisting.

```
var result = add(1,2);

function add(a, b){
    return a + b;
}
```

## Callback functions ##
Functions can be passed as arguments into other functions.
They can then be invoked inside the executing function.

```
function add(a, b){
    return a + b;
}

function mult(a, b){
    return a * b;
}

function calculate(a, b, f){
    return f(a, b);
}

var sum = calculate(1, 2, add);
var product = calculate(1, 2, mult);
```

## Pyramid of Doom ##
The anti-pattern known as the "Pyramid of Doom" is the result of multiple nested operations that produce a large number of blocks in the code.
The deeper the structure goes, the harder it is to understand the code and the easier it is to introduce errors.
By planning ahead and using outsourced functions the code can be properly "flattened".

```
badPyramidFunction{
    if (condition1) {
        dosomething
        if (condition2) {
            dosomething
            if (condition3) {
                dosomething
            }
            else {
                dosomethingelse
            }
        }
        else {
            dosomethingelse
        }
    }
    else {
        dosomethingelse
    }
}
```

### References ###
https://www.w3schools.com/js
https://medium.com/madhash/pyramid-of-doom-the-signs-and-symptoms-of-a-common-anti-pattern-c716838e1819