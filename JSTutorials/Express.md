# Creating a simple REST-based interface with Express #

## Express documentation ##
An extensiv documentation of Express can be found here: https://expressjs.com/de/  
Our very short tutorial will only show the quickest steps to get a REST interface up and running. For a more complete understanding the official documentation should be consulted.  

### Prerequisites ###
* node.js
* npm

### Installing Express ###
In your project directory run:  
```
npm init // make sure to use index.js as entrypoint
npm install express --save
```
in a file called *app.js* add the following lines to import Express:
```
const express = require('express')
const app = express()
const port = 5000
```  

Add the following code next:  
```
app.get('/', (req, res) => {
  res.send('Hallo, Integration Architectures. GET funktioniert.')
})

app.post('/', (req, res) => {
  res.send('POST auch.')
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
```  
This adds a GET and POST route to the express app and tells it to listen on port 5000.  
  
You now have a running Express app.  
  
Alternatively you can use the [Express generator](https://expressjs.com/de/starter/generator.html) to quickly set up an Express app.