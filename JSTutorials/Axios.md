# Axios #

## Introduction ##
Axios is a JavaScript library to perform HTTP requests in Node.js
To install Axios using npm, run the command

```
$ npm install axios
```

## REST with Axios ##
First you need to import Axios:
```
const axios = require('axios');  
```
To receive proper authorization, a bearer Token has to be generated and saved.
That is achieved by making a POST request with the required credentials and saving the bearer token globally as the authorization methods with the axios.defaults.header.common property.

```
async function assignAccessToken(requestTokenBody, requestTokenConfig) {
    const requestTokenResult = await axios.post(`${baseUrl}/oauth/issueToken`, requestTokenBody, requestTokenConfig)
        .then((res) => {
            accessToken = `Bearer ${res.data['access_token']}`;
            console.log(accessToken)
            axios.defaults.headers.common['Authorization'] = accessToken;
        })
        .catch((error) => {
            throw Error(error);
        })
}
```
To ensure the token is generated before any further requests, the method to assign the token will be invoked in the next request method. Because it is an <em>async</em> method, we use the keyword <em>await</em> to first wait for the token response.
What follows is a simple GET request on an Employee for OrangeHRM:
```
async function getEmployee(employeeID) {

    await assignAccessToken(requestTokenBody, requesTokenConfig);

    await axios.get(`${baseUrl}/api/v1/employee/${employeeID}`)
        .then((res) => {
            console.log(res)
        })
        .catch((error) => {
            throw Error(error)
        }
}

```
Here is a simple POST request to update an Employee's bonus salary in a given year:
```

async function updateBonusSalary(employeeID, body) {

    await assignAccessToken(requestTokenBody, requesTokenConfig);

    await axios.post(`${baseUrl}/api/v1/employee/${employeeID}/bonussalary`, body)
        .then((res) => {
            console.log(res)
        })
        .catch((error) => {
            throw Error(error)
        })
}

updateBonusSalary(2, queryString.stringify({
    year: 1990,
    value: 500
}));
```

The full code can be found [here](./Axios.js).

### References ###
https://www.npmjs.com/package/axios