# Promise #

## Definition ##
A JavaScript promise is an object that can be used instead of a value that currently might not be known.
The object "promises" that the required value will be available when it is needed and defines an action that will be taken if it is available but will also define an action that will be taken if it is not.
A promise object has a property <em>state</em> which can either be pending, fulfilled or rejected.

## Syntax ##
The promise object holds the producing code and takes a callback function with a <em>resolve</em> and a <em>reject</em> parameter.

```
var promise = new Promise(function(resolve, reject) {
  // code that may or may not produce the desired result 

  if (/* desired result */) {
    resolve("Promise fullfilled");
  }
  else {
    reject(Error("Error"));
  }
});
```

You can use the promise with the <em>then</em> parameter and providing a callback for success and one for failure. Both are optional.

```
promise.then(function(value){
    dosomething(value);
}, function(err){
    console.log(err);
});
```


### References ###
https://www.w3schools.com/js/js_promise.asp
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise