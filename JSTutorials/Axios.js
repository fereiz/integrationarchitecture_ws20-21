const axios = require('axios').default;
const queryString = require('querystring');

require('dotenv').config();

const baseUrl = 'https://sepp-hrm.inf.h-brs.de/symfony/web/index.php'
let accessToken = null;

const requestTokenBody = queryString.stringify({
    client_id: 'api_oauth_id',
    client_secret: 'oauth_secret',
    grant_type: 'password',
    username: process.env.ORANGE_HRM_USER,
    password: process.env.ORANGE_HRM_PASSWORD
});

const requesTokenConfig = {
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json',
    }
};

async function assignAccessToken(requestTokenBody, requestTokenConfig) {
    const requestTokenResult = await axios.post(`${baseUrl}/oauth/issueToken`, requestTokenBody, requestTokenConfig)
        .then((res) => {
            accessToken = `Bearer ${res.data['access_token']}`;
            console.log(accessToken)
            axios.defaults.headers.common['Authorization'] = accessToken;
        })
        .catch((error) => {
            throw Error(error);
        })
}

async function updateBonusSalary(employeeID, body) {

    await assignAccessToken(requestTokenBody, requesTokenConfig);

    await axios.post(`${baseUrl}/api/v1/employee/${employeeID}/bonussalary`, body)
        .then((res) => {
            console.log(res)
        })
        .catch((error) => {
            throw Error(error)
        })
}

async function getEmployee(employeeID) {

    await assignAccessToken(requestTokenBody, requesTokenConfig);

    await axios.get(`${baseUrl}/api/v1/employee/${employeeID}`)
        .then((res) => {
            console.log(res)
        })
        .catch((error) => {
            throw Error(error)
        })
}

updateBonusSalary(2, queryString.stringify({
    year: 1990,
    value: 500
}));

getEmployee(2);