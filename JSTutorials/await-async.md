# await/async #

## Prerequisites ##
[Promises](./Promise.md)

## async ##
In JavaSrcipt, the <em>async</em> keyword can be used make a function return a promise object.

```
async function asyncFunc(){
    return "Success";
}

asyncFunc().then(function(value){
    console.log(value);
});
```

## await ##
The <em>await</em> keyword is used to make an <em>async</em> function wait until a promise is fulfilled or rejected.

```
function longDurationFunc(){
    return new Promise(resolve){

    /*Complicated code that takes a long time to complete*/

        if (/* desired result */) {
            resolve("Promise fullfilled");
        }
    }
}

async function asyncFunc(){
    var result = await longDurationFunc();
    return result;
}

```

### References ###
https://www.w3schools.com/js/js_async.asp
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/await