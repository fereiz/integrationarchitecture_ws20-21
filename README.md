# Integration Architectures #
This repository contains our collaborative homework for the module Integration Architectures.

## What is this repository for? ##
As of now this repository contains multiple stages of our homework for the module Integration Architectures.

# Deprecated #
This directory contains early stages of this project which will not be maintained.

# Diagrams #
This directory contains a variety of UML diagrams as well as a written documentation of the entire project.

# High Performance API #
The directory ```high-performance-api``` contains a nodejs REST API written by adhering to [Robert Martin's Clean Architecture guidelines](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html).

## Why Clean Architecture ##
While developing our first iteration of the nodejs project, we encountered an issue. By focusing on third party frameworks like expressjs, monk, etc. we programmed ourselves into a corner. Unit tests were harder to write and TDD with tests written first seemed impossible whithout mocking entire APIs. One solution we found was the Clean Architecture by Robert Martin. Our implementation can be found in the corresponding repository.  
  
Personally, I would say that the testability of this project is greatly increased compared to our first approach. Another benefit is that our application does not depend on any third party frameworks.

# High Performance UI with Angular #
The frontend of the application, created with Angular.
Allows the user to interact with the API of the backend via a graphical user interface. 

# How do I get set up? #

## Prerequisites
- rename ```high-performance-api/.env.example``` to ```high-performance-api/.env``` and fill out missing credentials
- establish VPN connection to the H-BRS network

## Manually
- run ```npm install``` from the ```high-performance-api``` and  ```high-performance-ui``` directories
- run ```npm start``` from the ```high-performance-api``` and  ```high-performance-ui``` directories
You can now access the UI on localhost:4200 with the admin:admin user.

## Docker
The entire project can be build and run using the docker-compose file in the root directory.  
- run ```docker-compose up``` to spin up the static production deployment
- run ```docker-compose -f 'docker-compose.debug.yml' up``` to spin up the development version with hot-reloading for both front- and backend enabled


Docker exposes the following ports:
- 80 : the high performance ui
- 3000 : the high performance api
- 27017 : the mongo database
- 8081 : mongoexpress ui


# Frameworks used #
* Java :
    * Springboot to easily spin up a webservice
    * Lombok for injecting boilerplate code, so make sure your IDE is set up
* Nodejs :
    * Express for the webservice
    * monk to easily access the mongoDB
    * Joi for input validation
    * dotenv for using a dotenv file
* Nodejs-clean-architecture :
    * axios for end2end testing
    * babel for compiling es6
    * cuid to generate unique object ids
    * express to run the webservice
    * mongodb to interface with a mongo db
    * sanitize html to sanitize user input
    * jest, eslint, faker mongodb-memory-server, nodemon, standard for developing and testing
