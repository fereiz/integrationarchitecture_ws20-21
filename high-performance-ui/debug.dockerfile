FROM node:14

RUN npm set strict-ssl false

WORKDIR /high-performance-ui

COPY package.json ./

RUN npm i

COPY . .

EXPOSE 4200

CMD ["npm", "run", "start:dev"]