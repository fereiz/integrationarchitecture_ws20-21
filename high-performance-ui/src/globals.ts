export const BACK_END_API_URL: string = "http://localhost:3000/api/v1";
export const CURRENT_YEAR: number = new Date().getFullYear();