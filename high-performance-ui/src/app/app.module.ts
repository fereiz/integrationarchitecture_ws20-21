import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './routers/app-routing.module';
import { AppComponent } from './components/app/app.component';
import { EmployeeComponent } from './components/employee/employee.component';
import { RecordComponent } from './components/record/record.component';
import { DetailsComponent } from './components/details/details.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTableModule } from '@angular/material/table'
import { MatFormFieldModule } from '@angular/material/form-field';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { OrderEvaluationEditDialogComponent } from './components/order-evaluation-edit-dialog/order-evaluation-edit-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { SocialEvaluationAddDialogComponent } from './components/social-evaluation-add-dialog/social-evaluation-add-dialog.component';
import { SocialEvaluationEditDialogComponent } from './components/social-evaluation-edit-dialog/social-evaluation-edit-dialog.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { AuthHeaderInterceptor } from './interceptors/auth-header/auth-header.interceptor';
import { ErrorInterceptor } from './interceptors/error/error.interceptor';
import { RecordAddDialogComponent } from './components/record-add-dialog/record-add-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    EmployeeComponent,
    RecordComponent,
    DetailsComponent,
    OrderEvaluationEditDialogComponent,
    SocialEvaluationAddDialogComponent,
    SocialEvaluationEditDialogComponent,
    LoginComponent,
    HomeComponent,
    PageNotFoundComponent,
    RecordAddDialogComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatFormFieldModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    MatDialogModule
  ],

  entryComponents: [
    OrderEvaluationEditDialogComponent
  ],

  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthHeaderInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
