import OrderEvaluation from '../models/orderEvaluation'
import Record from '../models/record'
import SocialPerformanceEvaluation from '../models/socialPerformanceEvaluation'

const orderEval1: OrderEvaluation[] = [
    {productName: "HooverRoover", client: "Telekom", clientRanking: "Very Good", items: 20, bonus: 20, comment: "YES!"},
    {productName: "HooverRoover", client: "Telekom", clientRanking: "Very Good", items: 20, bonus: 20, comment: "YES!"},
    {productName: "HooverRoover", client: "Telekom", clientRanking: "Very Good", items: 20, bonus: 20, comment: "YES!"},
    {productName: "HooverRoover", client: "Telekom", clientRanking: "Very Good", items: 20, bonus: 20, comment: "YES!"},
    {productName: "HooverRoover", client: "Telekom", clientRanking: "Very Good", items: 20, bonus: 20, comment: "YES!"},
    {productName: "HooverRoover", client: "Telekom", clientRanking: "Very Good", items: 20, bonus: 20, comment: "YES!"}
]

const socialEval1: SocialPerformanceEvaluation[] = [
    {type: "Leadership Competence", targetValue: 4, actualValue: 5, bonus: 200, comment: "What's wrong?"},
    {type: "Leadership Competence", targetValue: 4, actualValue: 5, bonus: 200, comment: "What's wrong?"},
    {type: "Leadership Competence", targetValue: 4, actualValue: 5, bonus: 200, comment: "What's wrong?"},
    {type: "Leadership Competence", targetValue: 4, actualValue: 5, bonus: 200, comment: "What's wrong?"},
    {type: "Leadership Competence", targetValue: 4, actualValue: 5, bonus: 200, comment: "What's wrong?"},
    {type: "Leadership Competence", targetValue: 4, actualValue: 5, bonus: 200, comment: "What's wrong?"},
    {type: "Leadership Competence", targetValue: 4, actualValue: 5, bonus: 200, comment: "What's wrong?"}
]

export const RECORDS: Record[] = [
    //{id: "1", employeeId: "1", year: 2020, status: "Posted", orderEvaluations: orderEval1, socialPerformanceEvaluations: socialEval1},
    //{id: "2", employeeId: "1", year: 2019, status: "Posted", orderEvaluations: orderEval1, socialPerformanceEvaluations: socialEval1},
    //{id: "3", employeeId: "1", year: 2018, status: "Posted", orderEvaluations: orderEval1, socialPerformanceEvaluations: socialEval1}
]

