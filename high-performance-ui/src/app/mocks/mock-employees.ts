import Employee from '../models/employee'

export const EMPLOYEES: Employee[] = [
    {id: 1, fullName: "Joseph Joestar"},
    {id: 2, fullName: "Josuke Higashikata"},
    {id: 3, fullName: "Dio Brando"}
]