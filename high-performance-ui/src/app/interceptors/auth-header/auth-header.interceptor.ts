// Copied from https://jasonwatmore.com/post/2020/09/09/angular-10-role-based-authorization-tutorial-with-example and then modified

import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../../services/authentification/authentication.service';
import { BACK_END_API_URL } from '../../../globals';

@Injectable()
export class AuthHeaderInterceptor implements HttpInterceptor {

  constructor(private authenticationService: AuthenticationService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      // add auth header with jwt if user is logged in and request is to api url
      const user = this.authenticationService.userValue;
      const isLoggedIn = user && user.token;
      const isApiUrl = request.url.startsWith(BACK_END_API_URL);
      if (isLoggedIn && isApiUrl) {
          request = request.clone({
              setHeaders: {
                  Authorization: `Bearer ${user.token}`
              }
          });
      }

      return next.handle(request);
  }
}
