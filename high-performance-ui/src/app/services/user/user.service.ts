import { Injectable } from '@angular/core';
import { Role } from 'src/app/models/role';
import { User } from 'src/app/models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  currentUser!: User;

  constructor() {}
 
}
