import { Injectable, Output } from '@angular/core';
import Record from '../../models/record'
import { RECORDS } from '../../mocks/mock-records'
import { Observable, of } from 'rxjs';
import { EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { BACK_END_API_URL } from '../../../globals'
import SocialPerformanceEvaluation from '../../models/socialPerformanceEvaluation';

@Injectable({
  providedIn: 'root'
})
export default class RecordService {

  constructor(private http: HttpClient) { }

  private recordUrl = `${BACK_END_API_URL}/records`
  private bonusUrl = `${BACK_END_API_URL}/bonus`

  private selectedRecord!: Record | null;

  getSelectedRecord(): Record | null {
    return this.selectedRecord;
  }

  setSelectedRecord(record: Record | null) {
    this.selectedRecord = record;
    this.recordSelected.emit(record);
  }

  getRecordsByEmployeeId(id: number): Observable<Record[]> {
    return this.http.get<Record[]>(`${this.recordUrl}/${id}`);
  }

  addRecord(employeeId: number, year: number): Observable<any>{
    const body = {
      "employeeId": +employeeId,
      "department": "Sales",
      "year": year
    }
    return this.http.post<Record>(this.recordUrl, body);
  }

  updateRecord(record: Record): Observable<any>{
    record.modifiedOnDate = Date.now();
    return this.http.patch<Record>(`${this.recordUrl}/${record.id}`, record);
  }

  updateSocialPerformanceEvaluations(socialEvaluations: SocialPerformanceEvaluation[], recordId: string): Observable<any> {
    return this.http.patch<Record>(`${this.recordUrl}/${recordId}`, {
      "socialPerformanceEvaluations": socialEvaluations
    });
  }

  postBonus(record: Record): Observable<any>{
    return this.http.post<Record>(this.bonusUrl, record);
  }

  @Output() public recordSelected: EventEmitter<Record | null> = new EventEmitter();
}
