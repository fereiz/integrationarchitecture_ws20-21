import { Injectable, Output } from '@angular/core';
import Employee from '../../models/employee'
import { Observable } from 'rxjs';
import { EventEmitter } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BACK_END_API_URL } from '../../../globals'

@Injectable({
  providedIn: 'root'
})
export default class EmployeeService {

  constructor(private http: HttpClient) { }
  
  private employeeUrl = `${BACK_END_API_URL}/employees`

  private selectedEmployee!: Employee;

  getSelectedEmployee(): Employee {
    return this.selectedEmployee;
  }

  setSelectedEmployee(employee: Employee) {
    this.selectedEmployee = employee;
    this.employeeSelected.emit(this.selectedEmployee);
  }

  getEmployees(): Observable<Employee[]> {
    return this.http.get<Employee[]>(this.employeeUrl);
  }

  getEmployeeById(id: number | string): Observable<Employee[]> {
    return this.http.get<Employee[]>(this.employeeUrl, {
      params: new HttpParams().set("code", String(id))
    });
  }

  @Output() public employeeSelected: EventEmitter<Employee> = new EventEmitter();
}
