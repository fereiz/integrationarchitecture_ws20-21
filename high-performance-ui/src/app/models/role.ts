export enum Role {
    User = 'User',
    Admin = 'Admin',
    CEO = 'CEO',
    Salesman = 'Salesman',
    HR = 'HR'
}