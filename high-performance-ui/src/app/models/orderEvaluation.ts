export default interface OrderEvaluation {
    productName: string,
    client: string,
    clientRanking: string,
    items: number,
    bonus: number,
    comment: string
}