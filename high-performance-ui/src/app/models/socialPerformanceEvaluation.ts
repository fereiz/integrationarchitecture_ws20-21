export default interface SocialPerformanceEvaluation {
    type: string,
    targetValue: number,
    actualValue: number,
    bonus: number,
    comment: string
}