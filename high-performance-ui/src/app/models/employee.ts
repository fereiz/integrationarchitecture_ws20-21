export default interface Employee {
    id: number;
    fullName: string;
}