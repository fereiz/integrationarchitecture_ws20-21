import orderEvaluation from "./orderEvaluation";
import socialPerformanceEvaluation from "./socialPerformanceEvaluation";

export default interface Record {
    id: string,
    hash: string,
    employeeId: number,
    department: string,
    year: number,
    modifiedOnDate: number,
    remarks: string,
    bonusA: number,
    bonusB: number,
    totalBonus: 0,
    orderEvaluations: orderEvaluation[],
    socialPerformanceEvaluations: socialPerformanceEvaluation[];
}