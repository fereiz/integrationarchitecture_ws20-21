import { Component } from '@angular/core';
import { AuthenticationService } from '../../services/authentification/authentication.service';
import { User } from '../../models/user';
import { UserService } from 'src/app/services/user/user.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'High Performance';
  user!: User;

    constructor(private authenticationService: AuthenticationService, private userService: UserService) {
        this.authenticationService.user.subscribe(user => {
          this.userService.currentUser = user;
          this.user = user;
        });
    }

    logout() {
        this.authenticationService.logout();
    }
}
