import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SocialEvaluationAddDialogComponent } from './social-evaluation-add-dialog.component';

describe('SocialEvaluationAddDialogComponent', () => {
  let component: SocialEvaluationAddDialogComponent;
  let fixture: ComponentFixture<SocialEvaluationAddDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SocialEvaluationAddDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SocialEvaluationAddDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
