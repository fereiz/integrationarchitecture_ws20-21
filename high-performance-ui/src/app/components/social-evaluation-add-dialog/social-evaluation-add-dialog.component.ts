import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import Record from '../../models/record';
import RecordService from '../../services/record/record.service';
import SocialPerformanceEvaluation from '../../models/socialPerformanceEvaluation';

@Component({
  selector: 'app-social-evaluation-add-dialog',
  templateUrl: './social-evaluation-add-dialog.component.html',
  styleUrls: ['./social-evaluation-add-dialog.component.css']
})
export class SocialEvaluationAddDialogComponent implements OnInit {

  newSocialEvaluation: SocialPerformanceEvaluation = {
    type: "",
    targetValue: NaN,
    actualValue: NaN,
    bonus: NaN,
    comment: ""
  }

  constructor(private dialogRef: MatDialogRef<SocialEvaluationAddDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public recordData: Record,
    public recordService: RecordService) { }

  ngOnInit(): void {
  }

  cancel() {
    this.dialogRef.close();
  }

  submit() {
    let socialPerformanceEvaluations = this.recordService.getSelectedRecord()!.socialPerformanceEvaluations.concat(this.newSocialEvaluation);
    this.dialogRef.close(socialPerformanceEvaluations);
  }

  validate(){
    return !(
      this.newSocialEvaluation.type
      && (0 < this.newSocialEvaluation.targetValue && this.newSocialEvaluation.targetValue <= 5)
      && (0 < this.newSocialEvaluation.actualValue && this.newSocialEvaluation.actualValue <= 5)
    )
  }
}
