import { Component, OnInit } from '@angular/core';
import { filter } from 'rxjs/operators';
import Employee from '../../models/employee'
import EmployeeService from '../../services/employee/employee.service'
import RecordService from '../../services/record/record.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})

export class EmployeeComponent implements OnInit {

  employees!: Employee[];
  employeesAll!: Employee[];
  loading: boolean = false;

  constructor(private employeeService: EmployeeService, private recordService: RecordService) {
  }

  ngOnInit(): void {
    this.getEmployees();
  }

  getEmployees(): void {
    this.loading = true;
    this.employeeService.getEmployees()
      .subscribe(employees => {
        employees.sort((a: Employee, b: Employee) => {
          return a.fullName > b.fullName ? 1 : -1;
        });
        this.employeesAll = employees;
        this.employees = this.employeesAll;
        this.loading = false;
      });
  }

  refreshEmployees() {
    this.employees = [];
    this.getEmployees();
  }

  onSelect(employee: Employee): void {
    this.employeeService.setSelectedEmployee(employee);
    this.recordService.setSelectedRecord(null);
  }

  isSelected(employee: Employee): boolean {
    const selectedEmployee = this.employeeService.getSelectedEmployee();
    return selectedEmployee ? (selectedEmployee.id === employee.id) : false;
  }

  applyFilterOnInput(event: any): void {
    const filterValue = event.target.value;
    this.employees = this.filterEmployees(this.employeesAll, filterValue);
  }

  filterEmployees(employees: Employee[],filter: string) {
    return employees.filter(employee => employee.fullName.toLowerCase().includes(filter.toLowerCase()));
    }
  }
