import { Component, OnInit } from '@angular/core';
import { Role } from 'src/app/models/role';
import EmployeeService from 'src/app/services/employee/employee.service';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  isSalesman: boolean = (this.userService.currentUser.role == Role.Salesman);

  constructor(private userService: UserService, private employeeService: EmployeeService) { }

  ngOnInit(): void {
    if (this.isSalesman){
      this.employeeService.getEmployeeById(this.userService.currentUser.id).subscribe(employee => {
        this.employeeService.setSelectedEmployee(employee[0]);
      })
    }
  }
}
