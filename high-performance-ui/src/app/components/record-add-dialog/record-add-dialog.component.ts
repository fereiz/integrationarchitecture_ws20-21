import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import EmployeeService from 'src/app/services/employee/employee.service';
import RecordService from 'src/app/services/record/record.service';
import { CURRENT_YEAR } from 'src/globals';

@Component({
  selector: 'app-record-add-dialog',
  templateUrl: './record-add-dialog.component.html',
  styleUrls: ['./record-add-dialog.component.css']
})
export class RecordAddDialogComponent implements OnInit {

  currentYear: number = CURRENT_YEAR;
  loading: boolean = false;
  inputYear!: number;

  constructor(private dialogRef: MatDialogRef<RecordAddDialogComponent>, private employeeService: EmployeeService, private recordService: RecordService) { }

  ngOnInit(): void {
  }

  cancel() {
    this.dialogRef.close();
  }

  submit(year: number) {
    this.loading = true;
    this.recordService.addRecord(this.employeeService.getSelectedEmployee().id, year).subscribe(result => this.dialogRef.close(result.posted));
  }

}
