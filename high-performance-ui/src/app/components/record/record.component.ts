import { Component, OnInit } from '@angular/core';
import Record from '../../models/record'
import EmployeeService from '../../services/employee/employee.service';
import RecordService from '../../services/record/record.service';
import Employee from '../../models/employee';
import { UserService } from 'src/app/services/user/user.service';
import { Role } from 'src/app/models/role';
import { RecordAddDialogComponent } from '../record-add-dialog/record-add-dialog.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-record',
  templateUrl: './record.component.html',
  styleUrls: ['./record.component.css']
})
export class RecordComponent implements OnInit {

  records!: Record[];
  yearInput!: number;
  canAdd = [Role.Admin, Role.HR, Role.CEO].includes(this.userService.currentUser.role);
  loading: boolean = false;

  constructor(private employeeService: EmployeeService, private recordService: RecordService, private userService: UserService, public dialog: MatDialog) {
    this.employeeService.employeeSelected.subscribe((employee: Employee) => {
      this.loading = true;
      this.recordService.getRecordsByEmployeeId(employee.id).subscribe(records => {
        this.records = records;
        this.sortRecordsByYear(this.records);
        this.loading = false;
      });;
    })
  }

  ngOnInit(): void { }

  selectRecord(record: Record): void {
    this.recordService.setSelectedRecord(record);
  }

  openAddRecordDialog(): void {
    this.dialog.open(RecordAddDialogComponent, {
      disableClose: true
    }).afterClosed().subscribe((record: Record) => {
      if (record) this.addRecord(record);
    })
  }

  addRecord(record: Record) {
    if (this.records.map(record => record.year).includes(record.year)) {
      alert("Record already exists for that year.");
    } else {
      this.records.push(record);
      this.sortRecordsByYear(this.records);
    }
    this.selectRecord(record);
  }

  sortRecordsByYear(records: Record[]): void{
    records.sort((a: Record, b: Record) => {
      return a.year > b.year ? 1 : -1;
    })
  }

  isSelected(record: Record): boolean {
    const selectedRecord = this.recordService.getSelectedRecord();
    return selectedRecord ? (selectedRecord.id === record.id) : false;
  }

  employeeSelected(): boolean {
    return this.employeeService.getSelectedEmployee() ? true : false;
  }
}