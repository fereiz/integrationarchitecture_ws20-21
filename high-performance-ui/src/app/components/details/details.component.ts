import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { OrderEvaluationEditDialogComponent } from '../order-evaluation-edit-dialog/order-evaluation-edit-dialog.component';
import OrderEvaluation from '../../models/orderEvaluation';
import Record from '../../models/record';
import RecordService from '../../services/record/record.service';
import { SocialEvaluationAddDialogComponent } from '../social-evaluation-add-dialog/social-evaluation-add-dialog.component';
import { SocialEvaluationEditDialogComponent } from '../social-evaluation-edit-dialog/social-evaluation-edit-dialog.component';
import SocialPerformanceEvaluation from '../../models/socialPerformanceEvaluation';
import EmployeeService from 'src/app/services/employee/employee.service';
import { UserService } from 'src/app/services/user/user.service';
import { Role } from 'src/app/models/role';


@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {

  recordData!: Record;
  employeeName!: string;
  loadingSaveRemarks: boolean = false;
  loadingPostBonus: boolean = false;
  unsavedChanges!: boolean;

  // Permissions
  canEdit: boolean = false;
  canComment: boolean = false;
  canApprove: boolean = false;

  // Table headers
  orderColumns: string[] = [];
  socialColumns: string[] = [];

  headerKeysToNamesMap = new Map([
    ["productName", "Product Name"],
    ["client", "Client"],
    ["clientRanking", "Client Ranking"],
    ["items", "Items"],
    ["bonus", "Bonus"],
    ["type", "Type"],
    ["targetValue", "Target Value"],
    ["actualValue", "Actual Value"],
    ["comment", "Comment"]
  ])

  constructor(public employeeService: EmployeeService, public recordService: RecordService, public dialog: MatDialog, public userService: UserService) {
    this.recordService.recordSelected.subscribe((recordData: Record) => {
      if (recordData) {
        // Set permissions
        const role = userService.currentUser.role;
        this.canEdit = [Role.Admin, Role.HR].includes(role);
        this.canComment = [Role.Admin, Role.CEO].includes(role);
        this.canApprove = [Role.Admin, Role.CEO].includes(role);

        // Set table headers
        this.orderColumns = ["productName", "client", "clientRanking", "items", "bonus", "comment"];
        this.socialColumns = ["type", "targetValue", "actualValue", "bonus", "comment"];
        if (this.canEdit) {
          this.socialColumns.push("edit", "delete");
        }
        if (this.canComment) {
          this.orderColumns.push("editComment");
        }
        if (this.canComment && !this.canEdit) {
          this.socialColumns.push("edit");
        }
        this.employeeName = employeeService.getSelectedEmployee().fullName;
      }
      this.recordData = JSON.parse(JSON.stringify(recordData));
      this.unsavedChanges = false
    })
  }

  ngOnInit(): void {

  }

  truncate(value: number): number {
    return Math.trunc(value);
  }

  editOrderEvaluation(orderEvaluation: OrderEvaluation) {
    this.dialog.open(OrderEvaluationEditDialogComponent, {
      data: orderEvaluation,
      disableClose: true
    }).afterClosed().subscribe(edited => { if (edited) this.unsavedChanges = true });
  }

  editSocialEvaluation(socialEvaluation: SocialPerformanceEvaluation) {
    this.dialog.open(SocialEvaluationEditDialogComponent, {
      data: socialEvaluation,
      disableClose: true
    }).afterClosed().subscribe(edited => { if (edited) this.unsavedChanges = true });
  }

  addSocialEvaluation() {
    this.dialog.open(SocialEvaluationAddDialogComponent, {
      data: this.recordData,
      disableClose: true
    }).afterClosed().subscribe(socialPerformanceEvaluations => {
      if (socialPerformanceEvaluations) {
        this.recordData.socialPerformanceEvaluations = socialPerformanceEvaluations;
        this.unsavedChanges = true;
      }
    });
  }

  deleteSocialEvaluation(type: string) {
    this.recordData.socialPerformanceEvaluations = this.recordData.socialPerformanceEvaluations.filter(socialEval => socialEval.type !== type);
    this.unsavedChanges = true;
  }

  saveRecordAndCalcualateBonus() {
    this.loadingSaveRemarks = true;
    this.recordService.updateRecord(this.recordData).subscribe(response => {
      this.loadingSaveRemarks = false;
      if ("patched" in response) {
        this.recordData = response.patched;
        this.unsavedChanges = false;
        alert("Successfully saved");
      } else {
        alert("Something went wrong")
      }
    });
  }

  postRecord() {
    if (this.unsavedChanges) {
      alert("Please save your changes first");
    } else {
      this.loadingPostBonus = true;
      this.recordService.postBonus(this.recordData).subscribe(response => {
        this.loadingPostBonus = false;
        if ("posted" in response && "success" in response.posted) {
          alert("Successfully posted bonus");
        } else {
          alert("Something went wrong")
        }
      });
    }
  }
}
