import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Role } from 'src/app/models/role';
import { UserService } from 'src/app/services/user/user.service';
import SocialPerformanceEvaluation from '../../models/socialPerformanceEvaluation';

@Component({
  selector: 'app-social-evaluation-edit-dialog',
  templateUrl: './social-evaluation-edit-dialog.component.html',
  styleUrls: ['./social-evaluation-edit-dialog.component.css']
})
export class SocialEvaluationEditDialogComponent implements OnInit {

  canEdit: boolean = false;
  canComment: boolean = false;

  constructor(private dialogRef: MatDialogRef<SocialEvaluationEditDialogComponent>,public userService: UserService,
    @Inject(MAT_DIALOG_DATA) public socialEvaluation: SocialPerformanceEvaluation) {
      this.canEdit = [Role.Admin, Role.HR].includes(userService.currentUser.role);
      this.canComment = [Role.Admin, Role.CEO].includes(userService.currentUser.role);
     }

  ngOnInit(): void {
  }

  tempEvaluation: SocialPerformanceEvaluation = {
    type: this.socialEvaluation.type,
    targetValue: this.socialEvaluation.targetValue,
    actualValue: this.socialEvaluation.actualValue,
    bonus: this.socialEvaluation.bonus,
    comment: this.socialEvaluation.comment
  }


  cancel() {
    this.dialogRef.close();
  }

  submit() {
    Object.assign(this.socialEvaluation, this.tempEvaluation);
    this.dialogRef.close();
  }

}
