import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SocialEvaluationEditDialogComponent } from './social-evaluation-edit-dialog.component';

describe('SocialEvaluationEditDialogComponent', () => {
  let component: SocialEvaluationEditDialogComponent;
  let fixture: ComponentFixture<SocialEvaluationEditDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SocialEvaluationEditDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SocialEvaluationEditDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
