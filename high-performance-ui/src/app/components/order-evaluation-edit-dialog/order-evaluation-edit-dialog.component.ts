import { Component, OnInit } from '@angular/core';
import OrderEvaluation from '../../models/orderEvaluation';
import { Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import RecordService from 'src/app/services/record/record.service';

@Component({
  selector: 'app-order-evaluation-edit-dialog',
  templateUrl: './order-evaluation-edit-dialog.component.html',
  styleUrls: ['./order-evaluation-edit-dialog.component.css']
})
export class OrderEvaluationEditDialogComponent implements OnInit {

  constructor(private dialogRef: MatDialogRef<OrderEvaluationEditDialogComponent>, private recordService: RecordService,
    @Inject(MAT_DIALOG_DATA) public orderEvaluation: OrderEvaluation) { }

  ngOnInit(): void {
  }

  
  tempComment: string = this.orderEvaluation.comment;

  truncate(value: number): number {
    return Math.trunc(value);
  }

  cancel() {
    this.dialogRef.close();
  }

  submit() {
    this.orderEvaluation.comment = this.tempComment;
    this.dialogRef.close(true);
  }

}
