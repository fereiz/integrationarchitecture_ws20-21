import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderEvaluationEditDialogComponent } from './order-evaluation-edit-dialog.component';

describe('OrderEvaluationEditDialogComponent', () => {
  let component: OrderEvaluationEditDialogComponent;
  let fixture: ComponentFixture<OrderEvaluationEditDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrderEvaluationEditDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderEvaluationEditDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
