import { makeDb } from '../src/data-access'
import dotenv from 'dotenv'
dotenv.config()
;(async function setupDb () {
  console.log('Setting up database...')
  const db = await makeDb()
  const resultRecords = await db
    .collection('records')
    .createIndexes([
      { key: { hash: 1 }, name: 'hash_idx' },
      { key: { employeeId: -1 }, name: 'employeeId_idx' }
    ])
  const resultUsers = await db
    .collection('users')
    .insert({
      id: 1,
      username: 'admin',
      password: 'admin',
      firstName: 'Admin',
      lastName: 'User',
      role: 'Admin'
    })
  console.log(`Records DB: ${resultRecords}`)
  console.log(`Users DB: ${resultUsers}`)
  console.log('Database setup complete...')
  process.exit()
})()
