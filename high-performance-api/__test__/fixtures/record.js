import cuid from 'cuid'
import crypto from 'crypto'
import faker from 'faker'

const Id = Object.freeze({
  makeId: cuid,
  isValidId: cuid.isCuid
})

function md5 (text) {
  return crypto
    .createHash('md5')
    .update(text, 'utf-8')
    .digest('hex')
}

export default function makeFakeRecord (overrides) {
  const record = {
    id: Id.makeId(),
    employeeId: faker.random.number(),
    department: 'Sales',
    remarks: 'Keep it up!',
    year: 1984,
    orderEvaluations: [{
      productName: 'hoover roover',
      client: 'Deutsche Telekom AG',
      clientRanking: 5,
      items: 20,
      comment: 'Good job, keep it up!'
    },
    {
      productName: 'hoover roover v2',
      client: 'Vodafon',
      clientRanking: 2,
      items: 5,
      comment: 'Well done.'
    }],
    socialPerformanceEvaluations: [{
      type: 'Type 1',
      targetValue: 4,
      actualValue: 2,
      comment: 'Da geht noch mehr!'
    },
    {
      type: 'Type 2',
      targetValue: 4,
      actualValue: 1,
      comment: 'Darüber müssen wir aber mal sprechen'
    }]
  }

  record.hash = md5(
    record.employeeId +
    record.remarks +
    record.year +
    record.department
  )

  return {
    ...record,
    ...overrides
  }
}
