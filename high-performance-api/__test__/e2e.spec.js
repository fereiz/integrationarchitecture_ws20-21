import axios from 'axios'
import recordsDb, { makeDb } from '../src/data-access'
import makeFakeRecord from './fixtures/record'
import dotenv from 'dotenv'
dotenv.config()

describe('Records API', () => {
  beforeAll(() => {
    axios.defaults.baseURL = process.env.API_BASE_URL + process.env.API_ROOT
    axios.defaults.headers.common['Content-Type'] = 'application/json'
    axios.defaults.validateStatus = function (status) {
      return status < 500
    }
  })
  afterAll(async () => {
    const db = await makeDb()
    return db.collection('records').drop()
  })

  describe('adding records', () => {
    xit('adds a record to the database', async () => {
      const response = await axios.post(
        '/records/',
        makeFakeRecord({
          id: undefined,
          remarks: 'some test remarks'
        })
      )
      expect(response.status).toBe(201)
      const { posted } = response.data
      const doc = await recordsDb.findById(posted)
      expect(doc).toEqual(posted)
      return recordsDb.remove(posted)
    })
  })
  describe('listing records', () => {
    it('lists records', async () => {
      const record1 = makeFakeRecord()
      const record2 = makeFakeRecord()
      const records = [record1, record2]
      const inserts = await Promise.all(records.map(recordsDb.insert))
      const expected = [
        {
          ...record1,
          createdOn: inserts[0].createdOn
        },
        {
          ...record2,
          createdOn: inserts[1].createdOn
        }
      ]
      const response = await axios.get('/records/')
      expect(response.data).toContainEqual(expected[0])
      expect(response.data).toContainEqual(expected[1])
      return records.map(recordsDb.remove)
    })
  })
})
