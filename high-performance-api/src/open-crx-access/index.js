import makeSalesSource from './sales-source'
import axios from 'axios'

const ApiClient = axios.create({
  baseURL: process.env.OPEN_CRX_BASE_URL,
  headers: {
    'Content-Type': 'application/json'
  },
  auth: {
    username: process.env.OPEN_CRX_USER,
    password: process.env.OPEN_CRX_PASSWORD
  }
})

export function makeSource () {
  return {
    findAllSalesOrders () {
      return ApiClient.get('/org.opencrx.kernel.contract1/provider/CRX/segment/Standard/salesOrder')
    },
    findAccountById (id) {
      return ApiClient.get(`/org.opencrx.kernel.account1/provider/CRX/segment/Standard/account/${id}`)
    },
    findPositionsInOrders (id) {
      return ApiClient.get(`/org.opencrx.kernel.contract1/provider/CRX/segment/Standard/salesOrder/${id}/position`)
    }
  }
}

const salesSource = makeSalesSource({ makeSource })
export default salesSource
