export default function makeSalesSource ({ makeSource }) {
  return Object.freeze({
    findAllSalesOrders,
    findAccountById,
    findPositionsInOrders
  })

  async function findAllSalesOrders () {
    const source = await makeSource()
    const result = await source.findAllSalesOrders().then((rsp) => {
      return rsp.data.objects
    })
    return result
  }

  async function findAccountById ({ id }) {
    const source = await makeSource()
    const result = await source.findAccountById(id).then((rsp) => rsp.data)
    return result
  }

  async function findPositionsInOrders ({ id }) {
    const source = await makeSource()
    const result = await source.findPositionsInOrders(id).then((rsp) => rsp.data.objects)
    return result
  }
}
