import makeSalesSource from './sales-source'
import { makeSource } from './index'

describe('sales information source', () => {
  let salesSource

  beforeEach(async () => {
    salesSource = makeSalesSource({ makeSource })
  })

  it('lists all sales orders', async () => {
    const found = await salesSource.findAllSalesOrders()
    expect(found.length).not.toBe(0)
  })

  it('lists all info for a given account', async () => {
    const id = '9DXSJ5D62FBHLH2MA4T2TYJFL'
    const found = await salesSource.findAccountById({ id })
    expect(found.fullName).toEqual('Germania GmbH')
  })

  it('lists postitions for a given sales order', async () => {
    const id = '9DTSXR06DLHPM0EBHQA5MAZ7J'
    const found = await salesSource.findPositionsInOrders({ id })
    expect(found.length).not.toBe(0)
  })
})
