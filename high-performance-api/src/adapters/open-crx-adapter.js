import salesSource from '../open-crx-access'
import axios from 'axios'

export default async function supplySalesInfo (salesmanName) {
  const salesOrders = await salesSource.findAllSalesOrders()
  const orders = []
  const retArr = []
  salesOrders.forEach(order => orders.push({
    orderId: order.identity.replace('xri://@openmdx*org.opencrx.kernel.contract1/provider/CRX/segment/Standard/salesOrder/', ''),
    customerId: order.customer.$.replace('xri://@openmdx*org.opencrx.kernel.account1/provider/CRX/segment/Standard/account/', ''),
    salesRepId: order.salesRep.$.replace('xri://@openmdx*org.opencrx.kernel.account1/provider/CRX/segment/Standard/account/', '')
  }))

  for (const order of orders) {
    const positions = await getSimplePositions(order.orderId)
    const customer = await getCustomerNameAndRanking(order.customerId)
    const employeeId = await getEmployeeId(order.salesRepId)
    for (const position of positions) {
      retArr.push({
        employeeId: employeeId,
        productName: position.name,
        client: customer.customerName,
        clientRanking: customer.customerRanking,
        items: position.amount,
        identity: position.identity,
        year: position.year
      })
    }
  }

  return retArr
}

async function getEmployeeId (crxid, allEmployees) {
  const found = await salesSource.findAccountById({ id: crxid })
  return found.governmentId
}

async function getCustomerNameAndRanking (crxid) {
  const found = await salesSource.findAccountById({ id: crxid })
  return {
    customerName: found.fullName,
    customerRanking: found.accountRating
  }
}

async function getSimplePositions (crxid) {
  const found = await salesSource.findPositionsInOrders({ id: crxid })
  const retArr = []
  if (found) {
    for (const position of found) {
      const productName = await axios.get(position.product['@href'], {
        headers: {
          'Content-Type': 'application/json'
        },
        auth: {
          username: process.env.OPEN_CRX_USER,
          password: process.env.OPEN_CRX_PASSWORD
        }
      }).then((rsp) => rsp.data.name)
      retArr.push({ amount: position.quantity, name: productName, identity: position.identity, year: new Date(position.createdAt).getFullYear() })
    }
  }
  return retArr
}
