import express from 'express'
import bodyParser from 'body-parser'
import cors from 'cors'
import authorize from './authorize'
import roles from './roles'

import dotenv from 'dotenv'
import {
  deleteRecord,
  getRecords,
  notFound,
  postRecord,
  patchRecord,
  getEmployees,
  postBonus,
  postAuthenticateUser
} from './controllers'
import makeCallback from './express-callback'

dotenv.config()

const apiRoot = process.env.API_ROOT
const app = express()
app.use(bodyParser.json())
app.use(cors())

const management = [roles.Admin, roles.CEO, roles.HR]
const all = [roles.Admin, roles.CEO, roles.HR, roles.Salesman]

app.post(`${apiRoot}/records`, authorize(all), makeCallback(postRecord))
app.delete(`${apiRoot}/records/:id`, authorize(roles.Admin), makeCallback(deleteRecord))
app.patch(`${apiRoot}/records/:id`, authorize(management), makeCallback(patchRecord))
app.get(`${apiRoot}/records/:employeeId`, authorize(all), makeCallback(getRecords))
app.get(`${apiRoot}/records`, authorize(all), makeCallback(getRecords))
app.get(`${apiRoot}/employees`, authorize(all), makeCallback(getEmployees))
app.post(`${apiRoot}/bonus`, authorize([roles.CEO, roles.Admin]), makeCallback(postBonus))
app.post(`${apiRoot}/users/authenticate`, makeCallback(postAuthenticateUser))
app.use(makeCallback(notFound))

app.listen(3000, () => {
  ('Server is listening on port 3000')
})

export default app
