import makeRecordsDb from './records-db'
import mongodb from 'mongodb'
import axios from 'axios'
import makeEmployeeDb from './employee-db'
import queryString from 'querystring'
import makeUserDb from './user-db'

const MongoClient = mongodb.MongoClient
const url = process.env.MONGO_DB_URL
const dbName = process.env.MONGO_DB_NAME
const client = new MongoClient(url, { useNewUrlParser: true, useUnifiedTopology: true })

export async function makeDb () {
  if (!client.isConnected()) {
    await client.connect()
  }
  return client.db(dbName)
}

export async function makeHRM () {
  const instance = axios.create({
    baseURL: process.env.ORANGE_HRM_API_ROOT
  })
  await instance.post('/oauth/issueToken', queryString.stringify({
    client_id: 'api_oauth_id',
    client_secret: 'oauth_secret',
    grant_type: 'password',
    username: process.env.ORANGE_HRM_USER,
    password: process.env.ORANGE_HRM_PASSWORD
  }), {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      Accept: 'application/json'
    }
  }).then((res) => {
    instance.defaults.headers.common.Authorization = `Bearer ${res.data.access_token}`
  }).catch((error) => {
    throw Error(error)
  })

  return instance
}

const recordsDb = makeRecordsDb({ makeDb })
const employeeDb = makeEmployeeDb({ makeHRM })
const userDb = makeUserDb({ makeDb })
export { recordsDb, employeeDb, userDb }
