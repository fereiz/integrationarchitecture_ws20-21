import Id from '../Id'

export default function makeRecordsDb ({ makeDb }) {
  return Object.freeze({
    insert,
    findAll,
    findById,
    findByHash,
    findByEmployeeId,
    update,
    remove
  })

  async function findAll () {
    const db = await makeDb()
    const result = await db.collection('records').find({})
    return (await result.toArray()).map(({ _id: id, ...found }) => ({
      id,
      ...found
    }))
  }

  async function findByHash (record) {
    const db = await makeDb()
    const result = await db.collection('records').find({ hash: record.hash })
    const found = await result.toArray()
    if (found.length === 0) {
      return null
    }
    const { _id: id, ...foundInfo } = found[0]
    return { id, ...foundInfo }
  }

  async function findById ({ id: _id }) {
    const db = await makeDb()
    const result = await db.collection('records').find({ _id })
    const found = await result.toArray()
    if (found.length === 0) {
      return null
    }
    const { _id: id, ...info } = found[0]
    return { id, ...info }
  }

  async function findByEmployeeId ({ employeeId }) {
    const db = await makeDb()
    const result = await db.collection('records').find({ employeeId: +employeeId })
    const found = await result.toArray()
    return found.map(({ _id: id, ...found }) => ({
      id,
      ...found
    }))
  }

  async function update ({ id: _id, ...recordInfo }) {
    const db = await makeDb()
    const result = await db
      .collection('records')
      .updateOne({ _id }, { $set: { ...recordInfo } })
    return result.modifiedCount > 0 ? { id: _id, ...recordInfo } : null
  }

  async function insert ({ id: _id = Id.makeId(), ...recordInfo }) {
    const db = await makeDb()
    const result = await db
      .collection('records')
      .insertOne({ _id, ...recordInfo })
    const { _id: id, ...insertedInfo } = result.ops[0]
    return { id, ...insertedInfo }
  }

  async function remove ({ id: _id }) {
    const db = await makeDb()
    const result = await db.collection('records').deleteOne({ _id })
    return result.deletedCount
  }
}
