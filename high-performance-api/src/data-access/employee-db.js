export default function makeEmployeeDb ({ makeHRM }) {
  return Object.freeze({
    findAll,
    findById,
    updateBonusSalary
  })

  async function findAll () {
    const hrm = await makeHRM()
    const result = await hrm.get('/api/v1/employee/search')
    return result.data.data
  }

  async function findById ({ id: _id }) {
    const hrm = await makeHRM()
    const result = await hrm.get(`/api/v1/employee/${_id}`)
    return result.data.data
  }

  async function updateBonusSalary ({ employeeId, year, value }) {
    const hrm = await makeHRM()
    const result = await hrm.post(`/api/v1/employee/${employeeId}/bonussalary?year=${year}&value=${value}`)
    return result.data
  }
}
