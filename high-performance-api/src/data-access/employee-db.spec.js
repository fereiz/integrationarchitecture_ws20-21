import axios from 'axios'
import makeEmployeeDb from './employee-db'
import MockAdapter from 'axios-mock-adapter'

describe('employee source', () => {
  let employeeDb
  let mock

  beforeEach(async () => {
    const makeHRM = () => axios.create({ baseURL: process.env.ORANGE_HRM_API_ROOT })
    employeeDb = makeEmployeeDb({ makeHRM })

    mock = new MockAdapter(axios)
  })

  it('returns a list of all employees employees', async () => {
    const data = {
      data: [
        {
          firstName: 'Sascha',
          middleName: '',
          lastName: 'Alda',
          code: '98222',
          employeeId: '3',
          fullName: 'Sascha Alda',
          status: null,
          dob: null,
          driversLicenseNumber: '',
          licenseExpiryDate: null,
          maritalStatus: '',
          gender: null,
          otherId: '',
          nationality: null,
          unit: 'IT',
          jobTitle: 'External Consultant',
          supervisor: null
        },
        {
          firstName: 'Chantal',
          middleName: '',
          lastName: 'Banks',
          code: '90133',
          employeeId: '5',
          fullName: 'Chantal Banks',
          status: null,
          dob: null,
          driversLicenseNumber: '',
          licenseExpiryDate: null,
          maritalStatus: '',
          gender: null,
          otherId: '',
          nationality: null,
          unit: 'HR',
          jobTitle: 'HR Senior Consultant',
          supervisor: [
            {
              name: 'Michael Moore',
              id: '7'
            }
          ]
        },
        {
          firstName: 'John',
          middleName: '',
          lastName: 'Doe',
          code: '91338',
          employeeId: '85',
          fullName: 'John Doe',
          status: null,
          dob: null,
          driversLicenseNumber: '',
          licenseExpiryDate: null,
          maritalStatus: null,
          gender: null,
          otherId: '',
          nationality: null,
          unit: 'Sales',
          jobTitle: null,
          supervisor: null
        },
        {
          firstName: 'Tom',
          middleName: '',
          lastName: 'Foster',
          code: '91333',
          employeeId: '6',
          fullName: 'Tom Foster',
          status: null,
          dob: null,
          driversLicenseNumber: '',
          licenseExpiryDate: null,
          maritalStatus: '',
          gender: null,
          otherId: '',
          nationality: null,
          unit: 'IT',
          jobTitle: 'IT-admin',
          supervisor: null
        },
        {
          firstName: 'Paul',
          middleName: '',
          lastName: 'Kaye',
          code: '90732',
          employeeId: '31',
          fullName: 'Paul Kaye',
          status: null,
          dob: null,
          driversLicenseNumber: '',
          licenseExpiryDate: null,
          maritalStatus: null,
          gender: null,
          otherId: '',
          nationality: null,
          unit: 'Sales',
          jobTitle: 'Senior Salesman',
          supervisor: null
        },
        {
          firstName: 'Michael',
          middleName: '',
          lastName: 'Moore',
          code: '98777',
          employeeId: '7',
          fullName: 'Michael Moore',
          status: null,
          dob: null,
          driversLicenseNumber: '',
          licenseExpiryDate: null,
          maritalStatus: '',
          gender: null,
          otherId: '',
          nationality: null,
          unit: 'Leader',
          jobTitle: 'CEO',
          supervisor: null
        },
        {
          firstName: 'Mary-Ann',
          middleName: '',
          lastName: 'Sallinger',
          code: '90124',
          employeeId: '9',
          fullName: 'Mary-Ann Sallinger',
          status: null,
          dob: null,
          driversLicenseNumber: '',
          licenseExpiryDate: null,
          maritalStatus: '',
          gender: null,
          otherId: '',
          nationality: null,
          unit: 'Sales',
          jobTitle: 'Senior Salesman',
          supervisor: [
            {
              name: 'Michael Moore',
              id: '7'
            }
          ]
        },
        {
          firstName: 'John',
          middleName: 'Steven',
          lastName: 'Smith',
          code: '90123',
          employeeId: '2',
          fullName: 'John Steven Smith',
          status: null,
          dob: '1982-11-15',
          driversLicenseNumber: '',
          licenseExpiryDate: null,
          maritalStatus: '',
          gender: 'Male',
          otherId: '',
          nationality: 'British',
          unit: 'Sales',
          jobTitle: 'Senior Salesman',
          supervisor: [
            {
              name: 'Michael Moore',
              id: '7'
            }
          ]
        },
        {
          firstName: 'Toni',
          middleName: '',
          lastName: 'Tomato',
          code: '91337',
          employeeId: '84',
          fullName: 'Toni Tomato',
          status: null,
          dob: null,
          driversLicenseNumber: '',
          licenseExpiryDate: null,
          maritalStatus: '',
          gender: null,
          otherId: '',
          nationality: null,
          unit: 'Sales',
          jobTitle: null,
          supervisor: null
        },
        {
          firstName: 'Admin',
          middleName: '',
          lastName: 'User',
          code: '60999',
          employeeId: '1',
          fullName: 'Admin User',
          status: null,
          dob: null,
          driversLicenseNumber: '',
          licenseExpiryDate: null,
          maritalStatus: '',
          gender: null,
          otherId: '',
          nationality: null,
          unit: null,
          jobTitle: null,
          supervisor: null
        },
        {
          firstName: 'Demo',
          middleName: '',
          lastName: 'User',
          code: '60988',
          employeeId: '4',
          fullName: 'Demo User',
          status: null,
          dob: null,
          driversLicenseNumber: '',
          licenseExpiryDate: null,
          maritalStatus: '',
          gender: null,
          otherId: '',
          nationality: null,
          unit: null,
          jobTitle: null,
          supervisor: null
        }
      ],
      rels: []
    }
    const expected = [
      {
        firstName: 'Sascha',
        middleName: '',
        lastName: 'Alda',
        code: '98222',
        employeeId: '3',
        fullName: 'Sascha Alda',
        status: null,
        dob: null,
        driversLicenseNumber: '',
        licenseExpiryDate: null,
        maritalStatus: '',
        gender: null,
        otherId: '',
        nationality: null,
        unit: 'IT',
        jobTitle: 'External Consultant',
        supervisor: null
      },
      {
        firstName: 'Chantal',
        middleName: '',
        lastName: 'Banks',
        code: '90133',
        employeeId: '5',
        fullName: 'Chantal Banks',
        status: null,
        dob: null,
        driversLicenseNumber: '',
        licenseExpiryDate: null,
        maritalStatus: '',
        gender: null,
        otherId: '',
        nationality: null,
        unit: 'HR',
        jobTitle: 'HR Senior Consultant',
        supervisor: [
          {
            name: 'Michael Moore',
            id: '7'
          }
        ]
      },
      {
        firstName: 'John',
        middleName: '',
        lastName: 'Doe',
        code: '91338',
        employeeId: '85',
        fullName: 'John Doe',
        status: null,
        dob: null,
        driversLicenseNumber: '',
        licenseExpiryDate: null,
        maritalStatus: null,
        gender: null,
        otherId: '',
        nationality: null,
        unit: 'Sales',
        jobTitle: null,
        supervisor: null
      },
      {
        firstName: 'Tom',
        middleName: '',
        lastName: 'Foster',
        code: '91333',
        employeeId: '6',
        fullName: 'Tom Foster',
        status: null,
        dob: null,
        driversLicenseNumber: '',
        licenseExpiryDate: null,
        maritalStatus: '',
        gender: null,
        otherId: '',
        nationality: null,
        unit: 'IT',
        jobTitle: 'IT-admin',
        supervisor: null
      },
      {
        firstName: 'Paul',
        middleName: '',
        lastName: 'Kaye',
        code: '90732',
        employeeId: '31',
        fullName: 'Paul Kaye',
        status: null,
        dob: null,
        driversLicenseNumber: '',
        licenseExpiryDate: null,
        maritalStatus: null,
        gender: null,
        otherId: '',
        nationality: null,
        unit: 'Sales',
        jobTitle: 'Senior Salesman',
        supervisor: null
      },
      {
        firstName: 'Michael',
        middleName: '',
        lastName: 'Moore',
        code: '98777',
        employeeId: '7',
        fullName: 'Michael Moore',
        status: null,
        dob: null,
        driversLicenseNumber: '',
        licenseExpiryDate: null,
        maritalStatus: '',
        gender: null,
        otherId: '',
        nationality: null,
        unit: 'Leader',
        jobTitle: 'CEO',
        supervisor: null
      },
      {
        firstName: 'Mary-Ann',
        middleName: '',
        lastName: 'Sallinger',
        code: '90124',
        employeeId: '9',
        fullName: 'Mary-Ann Sallinger',
        status: null,
        dob: null,
        driversLicenseNumber: '',
        licenseExpiryDate: null,
        maritalStatus: '',
        gender: null,
        otherId: '',
        nationality: null,
        unit: 'Sales',
        jobTitle: 'Senior Salesman',
        supervisor: [
          {
            name: 'Michael Moore',
            id: '7'
          }
        ]
      },
      {
        firstName: 'John',
        middleName: 'Steven',
        lastName: 'Smith',
        code: '90123',
        employeeId: '2',
        fullName: 'John Steven Smith',
        status: null,
        dob: '1982-11-15',
        driversLicenseNumber: '',
        licenseExpiryDate: null,
        maritalStatus: '',
        gender: 'Male',
        otherId: '',
        nationality: 'British',
        unit: 'Sales',
        jobTitle: 'Senior Salesman',
        supervisor: [
          {
            name: 'Michael Moore',
            id: '7'
          }
        ]
      },
      {
        firstName: 'Toni',
        middleName: '',
        lastName: 'Tomato',
        code: '91337',
        employeeId: '84',
        fullName: 'Toni Tomato',
        status: null,
        dob: null,
        driversLicenseNumber: '',
        licenseExpiryDate: null,
        maritalStatus: '',
        gender: null,
        otherId: '',
        nationality: null,
        unit: 'Sales',
        jobTitle: null,
        supervisor: null
      },
      {
        firstName: 'Admin',
        middleName: '',
        lastName: 'User',
        code: '60999',
        employeeId: '1',
        fullName: 'Admin User',
        status: null,
        dob: null,
        driversLicenseNumber: '',
        licenseExpiryDate: null,
        maritalStatus: '',
        gender: null,
        otherId: '',
        nationality: null,
        unit: null,
        jobTitle: null,
        supervisor: null
      },
      {
        firstName: 'Demo',
        middleName: '',
        lastName: 'User',
        code: '60988',
        employeeId: '4',
        fullName: 'Demo User',
        status: null,
        dob: null,
        driversLicenseNumber: '',
        licenseExpiryDate: null,
        maritalStatus: '',
        gender: null,
        otherId: '',
        nationality: null,
        unit: null,
        jobTitle: null,
        supervisor: null
      }
    ]

    mock.onGet(`${process.env.ORANGE_HRM_API_ROOT}/api/v1/employee/search`).reply(200, data)

    const found = await employeeDb.findAll()
    expect(found).toEqual(expected)
  })

  it('returns a single employee by id', async () => {
    const data = {
      data: {
        firstName: 'Chantal',
        middleName: '',
        lastName: 'Banks',
        code: '90133',
        employeeId: '5',
        fullName: 'Chantal Banks',
        status: null,
        dob: null,
        driversLicenseNumber: '',
        licenseExpiryDate: null,
        maritalStatus: '',
        gender: null,
        otherId: '',
        nationality: null,
        unit: 'HR',
        jobTitle: 'HR Senior Consultant',
        supervisor: [
          {
            name: 'Michael Moore',
            id: '7'
          }
        ]
      },
      rels: {
        'contact-detail': '/employee/:id/contact-detail',
        'job-detail': '/employee/:id/job-detail',
        dependent: '/employee/:id/dependent',
        supervisor: '/employee/:id/supervisor'
      }
    }
    const expected = {
      firstName: 'Chantal',
      middleName: '',
      lastName: 'Banks',
      code: '90133',
      employeeId: '5',
      fullName: 'Chantal Banks',
      status: null,
      dob: null,
      driversLicenseNumber: '',
      licenseExpiryDate: null,
      maritalStatus: '',
      gender: null,
      otherId: '',
      nationality: null,
      unit: 'HR',
      jobTitle: 'HR Senior Consultant',
      supervisor: [
        {
          name: 'Michael Moore',
          id: '7'
        }
      ]
    }

    mock.onGet(`${process.env.ORANGE_HRM_API_ROOT}/api/v1/employee/5`).reply(200, data)

    const found = await employeeDb.findById({ id: 5 })
    expect(found).toEqual(expected)
  })

  it('updates bonus salary of one employee', async () => {
    const data = {
      success: 'Successfully Saved'
    }
    mock.onPost(`${process.env.ORANGE_HRM_API_ROOT}/api/v1/employee/9/bonussalary?year=2020&value=1005`).reply(200, data)
    const found = await employeeDb.updateBonusSalary({ employeeId: 9, year: 2020, value: 1005 })
    expect(found).toEqual(data)
  })
})
