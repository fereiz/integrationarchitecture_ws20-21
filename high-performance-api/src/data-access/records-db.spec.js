import makeFakeRecord from '../../__test__/fixtures/record'
import makeRecordsDb from './records-db'
import makeDb from '../../__test__/fixtures/db'

describe('records db', () => {
  let recordsDb

  beforeEach(async () => {
    recordsDb = makeRecordsDb({ makeDb })
  })

  it('lists records', async () => {
    const inserts = await Promise.all(
      [makeFakeRecord(), makeFakeRecord(), makeFakeRecord()].map(
        recordsDb.insert
      )
    )
    const found = await recordsDb.findAll()
    expect.assertions(inserts.length)
    return inserts.forEach(insert => expect(found).toContainEqual(insert))
  })

  it('inserts a record', async () => {
    const recordOne = makeFakeRecord()
    const recordTwo = makeFakeRecord({ id: undefined })
    const resultOne = await recordsDb.insert(recordOne)
    const resultTwo = await recordsDb.insert(recordTwo)
    expect(resultOne).toEqual(recordOne)
    expect(resultTwo.id).not.toBe(undefined)
  })

  it('finds a record by id', async () => {
    const record = makeFakeRecord()
    await recordsDb.insert(record)
    const found = await recordsDb.findById(record)
    expect(found).toEqual(record)
  })

  it("finds a record by it's hash", async () => {
    const fakeRecordOne = makeFakeRecord()
    const fakeRecordTwo = makeFakeRecord()
    const insertedOne = await recordsDb.insert(fakeRecordOne)
    const insertedTwo = await recordsDb.insert(fakeRecordTwo)

    expect(await recordsDb.findByHash(fakeRecordOne)).toEqual(insertedOne)
    expect(await recordsDb.findByHash(fakeRecordTwo)).toEqual(insertedTwo)
  })

  it('updates a record', async () => {
    const record = makeFakeRecord()
    await recordsDb.insert(record)
    record.remarks = 'changed'
    const updated = await recordsDb.update(record)
    return expect(updated.remarks).toBe('changed')
  })

  it('does not update if invalid object is given', async () => {
    const record = makeFakeRecord()
    const updated = await recordsDb.update(record)
    return expect(updated).toBe(null)
  })

  it('finds all records for a salesman', async () => {
    const recordsSalesmanA = makeFakeRecord({ year: 1000 })
    const recordsSalesmanAA = makeFakeRecord({ employeeId: recordsSalesmanA.employeeId, year: 1001 })
    const recordsSalesmanB = makeFakeRecord()
    const records = [recordsSalesmanA, recordsSalesmanAA, recordsSalesmanB]
    await Promise.all(records.map(recordsDb.insert))

    const returned = await recordsDb.findByEmployeeId({ employeeId: recordsSalesmanA.employeeId })

    expect(returned.filter((record) => record.id === recordsSalesmanA.id)[0]).toEqual(recordsSalesmanA)
    expect(returned.filter((record) => record.id === recordsSalesmanAA.id)[0]).toEqual(recordsSalesmanAA)
    expect(returned.filter((record) => record.id === recordsSalesmanB.id)[0]).toEqual(undefined)

    return Promise.all(records.map(recordsDb.remove))
  })

  it('deletes a record', async () => {
    const record = makeFakeRecord()
    await recordsDb.insert(record)
    return expect(await recordsDb.remove(record)).toBe(1)
  })
})
