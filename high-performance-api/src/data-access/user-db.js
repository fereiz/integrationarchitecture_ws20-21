export default function makeUserDb ({ makeDb }) {
  return Object.freeze({
    getAll,
    find
  })

  async function getAll () {
    const db = await makeDb()
    const result = await db.collection('users').find({})
    return (await result.toArray())
  }

  async function find ({ username, password }) {
    const db = await makeDb()
    const result = await db.collection('users').find({ username, password })
    const found = await result.toArray()
    if (found.length === 0) {
      return null
    }
    return found[0]
  }
}
