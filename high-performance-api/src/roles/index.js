const roles = {
  Admin: 'Admin',
  User: 'User',
  CEO: 'CEO',
  HR: 'HR',
  Salesman: 'Salesman'
}

export default roles
