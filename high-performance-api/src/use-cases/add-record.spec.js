import makeRecordsDb from '../data-access/records-db'
import makeDb from '../../__test__/fixtures/db'
import makeFakeRecord from '../../__test__/fixtures/record'
import makeAddRecord from './add-record'

describe('add record', () => {
  let recordsDb
  let addRecord
  beforeAll(() => {
    recordsDb = makeRecordsDb({ makeDb })
    addRecord = makeAddRecord({ recordsDb })
  })

  it('inserts records in the database', async () => {
    const newRecord = makeFakeRecord()
    const inserted = await addRecord(newRecord)
    expect(inserted).toMatchObject(newRecord)
  })

  it('it is idempotent', async () => {
    const newRecord = makeFakeRecord({ id: undefined })
    const insertOne = await addRecord(newRecord)
    const insertTwo = await addRecord(newRecord)
    expect(insertOne.id).toBeDefined()
    expect(insertOne.id).toBe(insertTwo.id)
  })
})
