export default function makePublishBonusSalary ({ employeeDb }) {
  return async function publishBonusSalary (recordInfo, orangeHRMID) {
    return employeeDb.updateBonusSalary({
      employeeId: orangeHRMID,
      year: recordInfo.year,
      value: recordInfo.totalBonus
    })
  }
}
