/* global describe, beforeAll, it, expect   */

import makeAuthenticateUser from './authenticate-user'

describe('authenticate-user', () => {
  let userDb
  beforeAll(() => {
    userDb = {
      getAll: () => {
        return [
          { id: 1, username: 'admin', password: 'admin', firstName: 'Admin', lastName: 'User', role: 'admin' },
          { id: 2, username: 'user', password: 'user', firstName: 'Normal', lastName: 'User', role: 'user' }
        ]
      },
      find: () => {
        return { id: 1, username: 'admin', password: 'admin', firstName: 'Admin', lastName: 'User', role: 'admin' }
      }
    }
  })
  it('returns a users data without the password', async () => {
    const authenticateUser = makeAuthenticateUser({ userDb, generateToken: () => {}, hashFunction: () => {} })
    const user = { username: 'admin', password: 'admin' }
    const actual = await authenticateUser(user)
    expect(actual).not.toHaveProperty('password')
  })

  it('returns a token if user was found', async () => {
    const authenticateUser = makeAuthenticateUser({ userDb, generateToken: () => {}, hashFunction: () => {} })
    const user = { username: 'admin', password: 'admin' }
    const actual = await authenticateUser(user)
    expect(actual).toHaveProperty('token')
  })
})
