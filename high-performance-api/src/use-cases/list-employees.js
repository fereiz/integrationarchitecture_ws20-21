export default function makeListEmployees ({ employeeDb }) {
  return async function listEmployees (query = null) {
    let employees
    if (query) {
      employees = await employeeDb.findAll()
      Object.entries(query).forEach(([key, value]) => {
        employees = employees.filter(employee => {
          return employee[key] === value
        })
      })
    } else {
      employees = await employeeDb.findAll()
    }

    return employees.filter((employee) => (employee.unit === 'Sales')).map((employee) => {
      return {
        id: employee.code,
        orangeHRMID: employee.employeeId,
        fullName: employee.fullName
      }
    }, [])
  }
}
