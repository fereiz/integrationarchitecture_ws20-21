import makeRecord from '../record'
import supplySalesInfo from '../adapters/open-crx-adapter'
export default function makeEditRecord ({ recordsDb }) {
  return async function editRecord ({ id, ...changes }) {
    if (!id) {
      throw new Error('You must supply an id')
    }
    /* if (!changes.employeeId) {
      throw new Error('You must supply an employee id')
    } */
    const existing = await recordsDb.findById({ id })

    if (!existing) {
      throw new RangeError('Record not found.')
    }
    const record = makeRecord({ ...existing, ...changes, modifiedOn: null })
    /* if (record.getHash() === existing.hash) {
      return existing
    } */

    const salesInfo = await supplySalesInfo()
    const orderEvaluations = record.getOrderEvaluations().map((evaluation) => {
      return {
        productName: evaluation.getProductName(),
        client: evaluation.getClient(),
        clientRanking: evaluation.getClientRanking(),
        items: evaluation.getItems(),
        comment: evaluation.getComment(),
        bonus: evaluation.getBonus(),
        identity: evaluation.getIdentity()
      }
    })
    salesInfo.forEach((info) => {
      if (info.employeeId == record.getEmployeeId() && info.year === record.getYear()) { // eslint-disable-line
        if (orderEvaluations.filter((evaluation) => evaluation.identity === info.identity).length === 0) {
          orderEvaluations.push(info)
        }
      }
    })

    const updated = await recordsDb.update({
      hash: record.getHash(),
      employeeId: record.getEmployeeId(),
      id: record.getId(),
      department: record.getDepartment(),
      year: record.getYear(),
      modifiedOnDate: record.getModifiedOnDate(),
      remarks: record.getRemarks(),
      bonusA: record.getBonusA(),
      bonusB: record.getBonusB(),
      totalBonus: record.getTotalBonus(),
      orderEvaluations: orderEvaluations,
      socialPerformanceEvaluations: record.getSocialPerformanceEvaluations().map((evaluation) => {
        return {
          type: evaluation.getType(),
          targetValue: evaluation.getTargetValue(),
          actualValue: evaluation.getActualValue(),
          comment: evaluation.getComment(),
          bonus: evaluation.getBonus()
        }
      })
    })
    return { ...existing, ...updated }
  }
}
