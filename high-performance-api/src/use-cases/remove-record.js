export default function makeRemoveRecord ({ recordsDb }) {
  return async function removeRecord ({ id }) {
    if (!id) {
      throw new Error('You must supply a record id')
    }

    const recordToDelete = await recordsDb.findById({ id })

    if (!recordToDelete) {
      return deleteNothing()
    }

    return deleteRecord(recordToDelete)
  }

  async function deleteNothing () {
    return {
      deletedCount: 0,
      message: 'Record not found, nothing to delete.'
    }
  }

  async function deleteRecord (record) {
    await recordsDb.remove(record)
    return {
      deletedCount: 1,
      message: 'Record deleted.'
    }
  }
}
