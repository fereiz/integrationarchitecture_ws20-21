import makeRecord from '../record'
import supplySalesInfo from '../adapters/open-crx-adapter'

export default function makeAddRecord ({ recordsDb }) {
  return async function addRecord (recordInfo) {
    const record = makeRecord(recordInfo)
    const exists = await recordsDb.findByHash({ hash: record.getHash() })
    if (exists) {
      return exists
    }

    const salesInfo = await supplySalesInfo()
    const orderEvaluations = record.getOrderEvaluations().map((evaluation) => {
      return {
        productName: evaluation.getProductName(),
        client: evaluation.getClient(),
        clientRanking: evaluation.getClientRanking(),
        items: evaluation.getItems(),
        comment: evaluation.getComment(),
        bonus: evaluation.getBonus(),
        identity: evaluation.getIdentity()
      }
    })
    salesInfo.forEach((info) => {
      if (info.employeeId == record.getEmployeeId() && info.year === record.getYear()) { // eslint-disable-line
        if (orderEvaluations.filter((evaluation) => evaluation.identity === info.identity).length === 0) {
          orderEvaluations.push(info)
        }
      }
    })

    const retRecord = makeRecord({ ...recordInfo, orderEvaluations })

    return recordsDb.insert({
      hash: retRecord.getHash(),
      employeeId: retRecord.getEmployeeId(),
      id: retRecord.getId(),
      department: retRecord.getDepartment(),
      year: retRecord.getYear(),
      modifiedOnDate: retRecord.getModifiedOnDate(),
      remarks: retRecord.getRemarks(),
      bonusA: retRecord.getBonusA(),
      bonusB: retRecord.getBonusB(),
      totalBonus: retRecord.getTotalBonus(),
      orderEvaluations: retRecord.getOrderEvaluations().map((evaluation) => {
        return {
          productName: evaluation.getProductName(),
          client: evaluation.getClient(),
          clientRanking: evaluation.getClientRanking(),
          items: evaluation.getItems(),
          comment: evaluation.getComment(),
          bonus: evaluation.getBonus(),
          identity: evaluation.getIdentity()
        }
      }),
      socialPerformanceEvaluations: retRecord.getSocialPerformanceEvaluations().map((evaluation) => {
        return {
          type: evaluation.getType(),
          targetValue: evaluation.getTargetValue(),
          actualValue: evaluation.getActualValue(),
          comment: evaluation.getComment(),
          bonus: evaluation.getBonus()
        }
      })
    })
  }
}
