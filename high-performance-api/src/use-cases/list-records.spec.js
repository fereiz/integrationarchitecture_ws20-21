import makeListRecords from './list-records'
import makeRecordsDb from '../data-access/records-db'
import makeFakeRecord from '../../__test__/fixtures/record'
import makeDb from '../../__test__/fixtures/db'

describe('get records', () => {
  let recordsDb, listRecords

  beforeAll(() => {
    recordsDb = makeRecordsDb({ makeDb })
    listRecords = makeListRecords({ recordsDb })
  })

  it('gets all records', async () => {
    const firstRecord = makeFakeRecord()
    const secondRecord = makeFakeRecord()
    const thirdRecord = makeFakeRecord()
    const records = [firstRecord, secondRecord, thirdRecord]
    await Promise.all(records.map(recordsDb.insert))
    const returned = await listRecords({})

    const firstRecordFromDb = returned.filter(r => r.id === firstRecord.id)
    expect(firstRecordFromDb[0].hash).toBe(firstRecord.hash)

    const secondRecordFromDb = returned.filter(r => r.id === secondRecord.id)
    expect(secondRecordFromDb[0].hash).toBe(secondRecord.hash)

    const thirdRecordFromDb = returned.filter(r => r.id === thirdRecord.id)
    expect(thirdRecordFromDb[0].hash).toBe(thirdRecord.hash)

    return Promise.all(records.map(recordsDb.remove))
  })

  it('gets all records for provided employeeId', async () => {
    const firstRecord = makeFakeRecord({ year: 1999 })
    const secondRecord = makeFakeRecord({ employeeId: firstRecord.employeeId, year: 2000 })
    const thirdRecord = makeFakeRecord()

    const records = [firstRecord, secondRecord, thirdRecord]
    await Promise.all(records.map(recordsDb.insert))
    const returned = await listRecords({ employeeId: firstRecord.employeeId })

    expect(returned.length).toBe(2)

    const firstRecordFromDb = returned.filter(r => r.id === firstRecord.id)
    expect(firstRecordFromDb[0].hash).toBe(firstRecord.hash)

    const secondRecordFromDb = returned.filter(r => r.id === secondRecord.id)
    expect(secondRecordFromDb[0].hash).toBe(secondRecord.hash)

    return Promise.all(records.map(recordsDb.remove))
  })
})
