import makeAddRecord from './add-record'
import makeEditRecord from './edit-record'
import makeRemoveRecord from './remove-record'
import makeListRecords from './list-records'
import { recordsDb, employeeDb, userDb } from '../data-access'
import makeListEmployees from './list-employees'
import makePublishBonusSalary from './publish-bonus-salary'
import makeAuthenticateUser from './authenticate-user'
import crypto from 'crypto'

import * as jwt from 'jsonwebtoken'

const generateToken = ({ id, role }) => {
  return jwt.sign({ id: id, role: role }, process.env.SECRET)
}

const hashFunction = (password) => {
  return crypto.createHash('sha256').update(password).update(process.env.SALT).digest('hex')
}

const addRecord = makeAddRecord({ recordsDb })
const editRecord = makeEditRecord({ recordsDb })
const listRecords = makeListRecords({ recordsDb })
const removeRecord = makeRemoveRecord({ recordsDb })

const listEmployees = makeListEmployees({ employeeDb })
const publishBonusSalary = makePublishBonusSalary({ employeeDb })

const authenticateUser = makeAuthenticateUser({ userDb, hashFunction, generateToken })

const recordService = Object.freeze({
  addRecord,
  editRecord,
  listRecords,
  removeRecord,
  listEmployees,
  publishBonusSalary,
  authenticateUser
})

export default recordService
export { addRecord, editRecord, listRecords, removeRecord, listEmployees, publishBonusSalary, authenticateUser }
