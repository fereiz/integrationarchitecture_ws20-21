import makeRemoveRecord from './remove-record'
import makeRecordsDb from '../data-access/records-db'
import makeFakeRecord from '../../__test__/fixtures/record'
import makeDb from '../../__test__/fixtures/db'

describe('remove records', () => {
  let recordsDb
  beforeAll(() => {
    recordsDb = makeRecordsDb({ makeDb })
  })

  it('handles non existent records', async () => {
    const removeRecord = makeRemoveRecord({ recordsDb })
    const fakeRecord = makeFakeRecord()
    const expected = {
      deletedCount: 0,
      message: 'Record not found, nothing to delete.'
    }
    const actual = await removeRecord(fakeRecord)
    expect(actual).toEqual(expected)
  })

  it('throws an error if no id is provided', () => {
    const removeRecord = makeRemoveRecord({ recordsDb })
    expect(removeRecord({ id: undefined })).rejects.toThrow('You must supply a record id')
  })

  it('deletes a record', async () => {
    const removeRecord = makeRemoveRecord({ recordsDb })

    const fakeRecord = makeFakeRecord()
    await recordsDb.insert(fakeRecord)

    const found = await recordsDb.findById(fakeRecord)
    expect(found).toEqual(fakeRecord)

    const expected = {
      deletedCount: 1,
      message: 'Record deleted.'
    }

    const actual = await removeRecord(fakeRecord)
    expect(actual).toEqual(expected)

    const notFound = await recordsDb.findById(fakeRecord)
    expect(notFound).toBe(null)
  })
})
