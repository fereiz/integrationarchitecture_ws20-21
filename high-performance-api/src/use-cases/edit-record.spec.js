import makeRecordsDb from '../data-access/records-db'
import makeDb from '../../__test__/fixtures/db'
import makeFakeRecord from '../../__test__/fixtures/record'
import makeEditRecord from './edit-record'

describe('edit record', () => {
  let recordsDb
  beforeAll(() => {
    recordsDb = makeRecordsDb({ makeDb })
  })

  it('must include an id', () => {
    const editRecord = makeEditRecord({
      recordsDb: {
        update: () => {
          throw new Error('update should not have been called')
        }
      }
    })
    const recordToEdit = makeFakeRecord({ id: undefined })
    expect(editRecord(recordToEdit)).rejects.toThrow('You must supply an id')
  })

  it('must throw a range error if not found in db', async () => {
    const editRecord = makeEditRecord({
      recordsDb
    })
    const recordToEdit = makeFakeRecord()
    expect(editRecord(recordToEdit)).rejects.toBeInstanceOf(RangeError)
  })

  xit('returns the existing record if the hash is the same', async () => {

  })

  xit('must include a employee id', () => {
    const editRecord = makeEditRecord({
      recordsDb: {
        update: () => {
          throw new Error('update shoud not have been called')
        }
      }
    })
    const recordToEdit = makeFakeRecord({ employeeId: undefined })
    expect(editRecord(recordToEdit)).rejects.toThrow('You must supply an employee id')
  })

  it('modifies a record', async () => {
    const editRecord = makeEditRecord({ recordsDb })
    const fakeRecord = makeFakeRecord({ modifiedOn: undefined })
    const inserted = await recordsDb.insert(fakeRecord)
    const edited = await editRecord({ ...fakeRecord, remarks: 'changed' })
    expect(edited.remarks).toBe('changed')
    expect(inserted.modifiedOn).not.toBe(edited.modifiedOn)
    expect(edited.hash).toBeDefined()
    expect(inserted.hash).not.toBe(edited.hash)
  })
})
