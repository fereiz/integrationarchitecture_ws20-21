export default function makeAuthenticateUser ({ userDb, hashFunction, generateToken }) {
  return async function authenticateUser ({ username, password }) {
    password = hashFunction(password)
    const user = await userDb.find({ username, password })
    if (user) {
      const token = generateToken({ id: user.id, role: user.role })
      const { password, _id, ...userWithoutPassword } = user
      return {
        ...userWithoutPassword,
        token
      }
    } else {
      throw new Error('User could not be authenticated')
    }
  }
}
