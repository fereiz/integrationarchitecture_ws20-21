export default function makeListRecords ({ recordsDb }) {
  return async function listRecords ({ employeeId }) {
    let records
    if (employeeId) {
      records = await recordsDb.findByEmployeeId({ employeeId })
    } else {
      records = await recordsDb.findAll()
    }
    return records
  }
}
