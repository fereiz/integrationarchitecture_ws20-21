import makeFakeRecord from '../../__test__/fixtures/record'
import makeGetRecords from './get-records'

describe('get records controller', () => {
  it('successfully returns all records', async () => {
    const fakeRecordOne = makeFakeRecord()
    const fakeRecordTwo = makeFakeRecord()
    const records = [fakeRecordOne, fakeRecordTwo]
    const getRecords = makeGetRecords({ listRecords: _ => records })
    const request = {
      headers: {
        'Content-Type': 'application/json'
      }
    }
    const expected = {
      headers: {
        'Content-Type': 'application/json'
      },
      statusCode: 200,
      body: records
    }
    const actual = await getRecords(request)
    expect(actual).toEqual(expected)
    expect(actual.body.length).toEqual(records.length)
  })

  it('returns records for given employeeId, if provided', async () => {
    const fakeRecordOne = makeFakeRecord()
    const fakeRecordTwo = makeFakeRecord()
    const records = [fakeRecordOne, fakeRecordTwo]
    const getRecords = makeGetRecords({
      listRecords: ({ employeeId }) => {
        return records.filter(record => record.employeeId === employeeId)
      }
    })
    const request = {
      headers: {
        'Content-Type': 'application/json'
      },
      params: {
        employeeId: fakeRecordOne.employeeId
      }
    }
    const expected = {
      headers: {
        'Content-Type': 'application/json'
      },
      statusCode: 200,
      body: records.filter(record => record.employeeId === fakeRecordOne.employeeId)
    }
    const actual = await getRecords(request)
    expect(actual).toEqual(expected)
  })

  it('reports user errors', async () => {
    const getRecords = makeGetRecords({
      listRecords: () => {
        throw Error('Error...!')
      }
    })
    const request = {
      headers: {
        'Content-Type': 'application/json'
      }
    }
    const expected = {
      headers: {
        'Content-Type': 'application/json'
      },
      statusCode: 400,
      body: { error: 'Error...!' }
    }
    const actual = await getRecords(request)
    expect(actual).toEqual(expected)
  })
})
