import makeFakeRecord from '../../__test__/fixtures/record'
import makeDeleteRecord from './delete-record'

describe('delete record controller', () => {
  it('successfully deletes a record', async () => {
    const fakeRecord = makeFakeRecord()
    const deleteRecord = makeDeleteRecord({ removeRecord: _ => fakeRecord })
    const request = {
      headers: {
        'Content-Type': 'application/json'
      },
      params: {
        id: fakeRecord.id
      },
      body: fakeRecord
    }
    const expected = {
      headers: {
        'Content-Type': 'application/json'
      },
      statusCode: 200,
      body: { deleted: request.body }
    }
    const actual = await deleteRecord(request)
    expect(actual).toEqual(expected)
  })

  it('returns a 404 if no record is found', async () => {
    const deleteRecord = makeDeleteRecord({ removeRecord: function () { return { deletedCount: 0 } } })
    const request = {
      headers: {
        'Content-Type': 'application/json'
      },
      params: {
        id: null
      },
      body: {}
    }
    const expected = {
      headers: {
        'Content-Type': 'application/json'
      },
      statusCode: 404,
      body: { deleted: { deletedCount: 0 } }
    }
    const actual = await deleteRecord(request)
    expect(actual).toEqual(expected)
  })

  it('reports user errors', async () => {
    const fakeRecord = makeFakeRecord()
    const deleteRecord = makeDeleteRecord({
      removeRecord: () => {
        throw Error('Error...!')
      }
    })
    const request = {
      headers: {
        'Content-Type': 'application/json'
      },
      params: {
        id: fakeRecord.id
      },
      body: fakeRecord
    }
    const expected = {
      headers: {
        'Content-Type': 'application/json'
      },
      statusCode: 400,
      body: { error: 'Error...!' }
    }
    const actual = await deleteRecord(request)
    expect(actual).toEqual(expected)
  })
})
