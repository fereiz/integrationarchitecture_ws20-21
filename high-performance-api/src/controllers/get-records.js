export default function makeGetRecords ({ listRecords }) {
  return async function getRecords (httpRequest) {
    const headers = {
      'Content-Type': 'application/json'
    }
    try {
      let postRecords
      if (httpRequest.params && httpRequest.params.employeeId) {
        postRecords = await listRecords({ employeeId: httpRequest.params.employeeId })
      } else {
        postRecords = await listRecords({})
      }
      return {
        headers,
        statusCode: 200,
        body: postRecords
      }
    } catch (e) {
      return {
        headers,
        statusCode: 400,
        body: {
          error: e.message
        }
      }
    }
  }
}
