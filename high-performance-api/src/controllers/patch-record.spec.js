import makeFakeRecord from '../../__test__/fixtures/record'
import makePatchRecord from './patch-record'

describe('patch record controller', () => {
  it('successfully patches a record', async () => {
    const fakeRecord = makeFakeRecord()
    const patchRecord = makePatchRecord({ editRecord: r => r })
    const request = {
      headers: {
        'Content-Type': 'application/json'
      },
      params: {
        id: fakeRecord.id
      },
      body: fakeRecord
    }
    const expected = {
      headers: {
        'Content-Type': 'application/json',
        'Last-Modified': new Date(fakeRecord.modifiedOn).toUTCString()
      },
      statusCode: 200,
      body: { patched: request.body }
    }
    const actual = await patchRecord(request)
    expect(actual).toEqual(expected)
  })

  it('reports a range error with code 404', async () => {
    const fakeRecord = makeFakeRecord()
    const patchRecord = makePatchRecord({
      editRecord: () => {
        throw RangeError('Error...!')
      }
    })
    const request = {
      headers: {
        'Content-Type': 'application/json'
      },
      params: {
        id: fakeRecord.id
      },
      body: fakeRecord
    }
    const expected = {
      headers: {
        'Content-Type': 'application/json'
      },
      statusCode: 404,
      body: { error: 'Error...!' }
    }
    const actual = await patchRecord(request)
    expect(actual).toEqual(expected)
  })

  it('reports user errors', async () => {
    const fakeRecord = makeFakeRecord()
    const patchRecord = makePatchRecord({
      editRecord: () => {
        throw Error('Error...!')
      }
    })
    const request = {
      headers: {
        'Content-Type': 'application/json'
      },
      params: {
        id: fakeRecord.id
      },
      body: fakeRecord
    }
    const expected = {
      headers: {
        'Content-Type': 'application/json'
      },
      statusCode: 400,
      body: { error: 'Error...!' }
    }
    const actual = await patchRecord(request)
    expect(actual).toEqual(expected)
  })
})
