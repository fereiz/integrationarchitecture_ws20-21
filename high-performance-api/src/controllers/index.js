import {
  addRecord,
  editRecord,
  listRecords,
  removeRecord,
  listEmployees,
  publishBonusSalary,
  authenticateUser
} from '../use-cases'
import makeDeleteRecord from './delete-record'
import makeGetRecords from './get-records'
import makePostRecord from './post-record'
import makePatchRecord from './patch-record'
import makeGetEmployees from './get-employees'
import makePostBonus from './post-bonus'
import notFound from './not-found'
import makePostAuthenticateUser from './post-authenticate-user'

const deleteRecord = makeDeleteRecord({ removeRecord })
const getRecords = makeGetRecords({ listRecords })
const postRecord = makePostRecord({ addRecord })
const patchRecord = makePatchRecord({ editRecord })
const getEmployees = makeGetEmployees({ listEmployees })
const postBonus = makePostBonus({ publishBonusSalary, listEmployees })
const postAuthenticateUser = makePostAuthenticateUser({ authenticateUser })

const recordController = Object.freeze({
  deleteRecord,
  getRecords,
  notFound,
  postRecord,
  getEmployees,
  patchRecord,
  postBonus,
  postAuthenticateUser
})

export default recordController
export { deleteRecord, getRecords, notFound, postRecord, patchRecord, getEmployees, postBonus, postAuthenticateUser }
