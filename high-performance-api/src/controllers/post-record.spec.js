import makePostRecord from './post-record'
import makeFakeRecord from '../../__test__/fixtures/record'

describe('post record controller', () => {
  it('successfully posts a record', async () => {
    const postRecord = makePostRecord({ addRecord: c => c })
    const record = makeFakeRecord()
    const request = {
      headers: {
        'Content-Type': 'application/json'
      },
      body: record
    }
    const expected = {
      headers: {
        'Content-Type': 'application/json',
        'Last-Modified': new Date(request.body.modifiedOn).toUTCString()
      },
      statusCode: 201,
      body: { posted: request.body }
    }
    const actual = await postRecord(request)
    expect(actual).toEqual(expected)
  })

  it('reports user errors', async () => {
    const postRecord = makePostRecord({
      addRecord: () => {
        throw Error('ERROR ERROR!')
      }
    })
    const fakeRecord = makeFakeRecord()
    const request = {
      headers: {
        'Content-Type': 'application/json'
      },
      body: fakeRecord
    }
    const expected = {
      headers: {
        'Content-Type': 'application/json'
      },
      statusCode: 400,
      body: { error: 'ERROR ERROR!' }
    }
    const actual = await postRecord(request)
    expect(actual).toEqual(expected)
  })
})
