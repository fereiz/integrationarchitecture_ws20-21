export default function makePatchRecord ({ editRecord }) {
  return async function patchRecord (httpRequest) {
    try {
      const { ...recordInfo } = httpRequest.body
      const toEdit = {
        ...recordInfo,
        id: httpRequest.params.id
      }
      const patched = await editRecord(toEdit)
      return {
        headers: {
          'Content-Type': 'application/json',
          'Last-Modified': new Date(patched.modifiedOn).toUTCString()
        },
        statusCode: 200,
        body: { patched }
      }
    } catch (e) {
      if (e.name === 'RangeError') {
        return {
          headers: {
            'Content-Type': 'application/json'
          },
          statusCode: 404,
          body: {
            error: e.message
          }
        }
      }
      return {
        headers: {
          'Content-Type': 'application/json'
        },
        statusCode: 400,
        body: {
          error: e.message
        }
      }
    }
  }
}
