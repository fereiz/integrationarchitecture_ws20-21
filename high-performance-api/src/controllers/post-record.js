export default function makePostRecord ({ addRecord }) {
  return async function postRecord (httpRequest) {
    try {
      const { ...recordInfo } = httpRequest.body
      const posted = await addRecord({
        ...recordInfo
      })
      return {
        headers: {
          'Content-Type': 'application/json',
          'Last-Modified': new Date(posted.modifiedOn).toUTCString()
        },
        statusCode: 201,
        body: { posted }
      }
    } catch (e) {
      return {
        headers: {
          'Content-Type': 'application/json'
        },
        statusCode: 400,
        body: {
          error: e.message
        }
      }
    }
  }
}
