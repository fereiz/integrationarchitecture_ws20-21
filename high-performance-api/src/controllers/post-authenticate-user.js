export default function makePostAuthenticUser ({ authenticateUser }) {
  return async function postAuthenticateUser (httpRequest) {
    try {
      const { ...user } = httpRequest.body
      const posted = await authenticateUser({
        ...user
      })
      return {
        headers: {
          'Content-Type': 'application/json'
        },
        statusCode: 200,
        body: { posted }
      }
    } catch (e) {
      return {
        headers: {
          'Content-Type': 'application/json'
        },
        statuscode: 400,
        body: {
          error: e.message
        }
      }
    }
  }
}
