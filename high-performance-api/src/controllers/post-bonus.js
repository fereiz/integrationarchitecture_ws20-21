export default function makePostBonus ({ publishBonusSalary, listEmployees }) {
  return async function postbonus (httpRequest) {
    try {
      const { ...recordInfo } = httpRequest.body
      const orangeHRMID = await listEmployees({ code: recordInfo.employeeId.toString() })
      const posted = await publishBonusSalary({
        ...recordInfo
      }, Number.parseInt(orangeHRMID[0].orangeHRMID))
      return {
        headers: {
          'Content-Type': 'application/json'
        },
        statusCode: 200,
        body: { posted }
      }
    } catch (e) {
      return {
        headers: {
          'Content-Type': 'application/json'
        },
        statusCode: 400,
        body: {
          error: e.message
        }
      }
    }
  }
}
