export default function makeGetEmployees ({ listEmployees }) {
  return async function getEmployees (httpRequest) {
    const headers = {
      'Content-Type': 'application/json'
    }
    try {
      let postEmployees
      if (httpRequest.query) {
        postEmployees = await listEmployees(httpRequest.query)
      } else {
        postEmployees = await listEmployees()
      }
      return {
        headers,
        statusCode: 200,
        body: postEmployees
      }
    } catch (e) {
      return {
        headers,
        statusCode: 400,
        body: {
          error: e.message
        }
      }
    }
  }
}
