import makeGetEmployees from './get-employees'

describe('get employees controller', () => {
  it('successfully returns all records', async () => {
    const employees = [{}, {}]
    const getEmployees = makeGetEmployees({ listEmployees: _ => employees })
    const request = {
      headers: {
        'Content-Type': 'application/json'
      }
    }
    const expected = {
      headers: {
        'Content-Type': 'application/json'
      },
      statusCode: 200,
      body: employees
    }
    const actual = await getEmployees(request)
    expect(actual).toEqual(expected)
    expect(actual.body.length).toEqual(employees.length)
  })
})
