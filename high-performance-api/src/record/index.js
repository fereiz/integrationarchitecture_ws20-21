import buildMakeRecord from './record'
import buildMakeOrderEvaluation from './orderEvaluation'
import buildMakeSocialPerformanceEvaluation from './socialPerformanceEvaluation'
import Id from '../Id'
import sanitizeHtml from 'sanitize-html'
import crypto from 'crypto'

const makeOrderEvaluation = buildMakeOrderEvaluation()
const makeSocialPerformanceEvaluation = buildMakeSocialPerformanceEvaluation()
const makeRecord = buildMakeRecord({ Id, sanitize, md5, makeOrderEvaluation, makeSocialPerformanceEvaluation, calculateTotalBonus })

export default makeRecord

function sanitize (text) {
  return sanitizeHtml(text)
}

function md5 (text) {
  return crypto
    .createHash('md5')
    .update(text, 'utf-8')
    .digest('hex')
}

function calculateTotalBonus (bonusA, bonusB) {
  return bonusA + bonusB
}
