export default function buildMakeOrderEvaluation () {
  return function makeOrderEvaluation ({
    productName,
    client,
    clientRanking,
    items,
    comment,
    identity
  } = {}) {
    if (!productName) {
      throw new Error('Order Evaluation Post needs a product name')
    }
    if (!client) {
      throw new Error('Order Evaluation Post needs a client')
    }
    if (!clientRanking) {
      throw new Error('Order Evaluation Post needs a client ranking')
    }
    if (!items) {
      throw new Error('Order Evaluation Post needs an amount of items')
    }

    const bonus = calculateBonus(clientRanking)

    return Object.freeze({
      getProductName: () => productName,
      getClient: () => client,
      getClientRanking: () => clientRanking,
      getItems: () => items,
      getComment: () => comment || '',
      getBonus: () => bonus,
      getIdentity: () => identity
    })

    function calculateBonus (ranking) {
      switch (ranking) {
        case 1:
          return 700
        case 2:
          return 500
        case 3:
          return 200
        case 4:
          return 100
        default:
          return 0
      }
    }
  }
}
