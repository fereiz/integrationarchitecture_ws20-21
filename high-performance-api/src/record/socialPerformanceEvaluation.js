export default function buildMakeSocialPerformanceEvaluation () {
  return function makeSocialPerformanceEvaluation ({
    type,
    targetValue,
    actualValue,
    comment
  } = {}) {
    if (!type) {
      throw new Error('Social Performance Evaluation Post needs a type')
    }
    if (!targetValue) {
      throw new Error('Social Performance Post needs a target value')
    }
    if (!actualValue) {
      throw new Error('Social Performance Post needs a actual value')
    }

    const bonus = calculateBonus(targetValue, actualValue)

    return Object.freeze({
      getType: () => type,
      getTargetValue: () => targetValue,
      getActualValue: () => actualValue,
      getComment: () => comment || '',
      getBonus: () => bonus
    })

    function calculateBonus (targetValue, actualValue) {
      const ratio = actualValue / targetValue
      switch (true) {
        case (ratio < 0.75):
          return 10
        case (ratio < 1):
          return 20
        case (ratio === 1):
          return 50
        case (ratio > 1):
          return 100
        default:
          return 0
      }
    }
  }
}
