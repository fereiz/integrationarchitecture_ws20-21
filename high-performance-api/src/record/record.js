export default function buildMakeRecord ({ Id, sanitize, md5, makeOrderEvaluation, makeSocialPerformanceEvaluation, calculateTotalBonus }) {
  return function makeRecord ({
    employeeId,
    id = Id.makeId(),
    department,
    year = new Date().getFullYear(),
    modifiedOnDate = Date.now(),
    remarks = '',
    orderEvaluations,
    socialPerformanceEvaluations
  } = {}) {
    if (!employeeId) {
      throw new Error('Record must have an employeeId')
    }
    if (!Id.isValidId(id)) {
      throw new Error('Record must have a valid id')
    }
    if (!department || department.length < 1) {
      throw new Error('Department must have at least one character.')
    }
    if (orderEvaluations && !Array.isArray(orderEvaluations)) {
      throw new Error('Record must have valid order evaluations')
    }
    if (socialPerformanceEvaluations && !Array.isArray(socialPerformanceEvaluations)) {
      throw new Error('Record must have valid social performance evaluations')
    }

    const validOrderEvaluations = validateEvaluations(orderEvaluations, 'orderEvaluations')
    const validSocialPerformanceEvaluations = validateEvaluations(socialPerformanceEvaluations, 'socialPerformanceEvaluations')
    const sanitizedRemarks = sanitize(remarks).trim()
    const bonusA = calculateTotalSubBonus(validOrderEvaluations)
    const bonusB = calculateTotalSubBonus(validSocialPerformanceEvaluations)
    const totalBonus = calculateTotalBonus(bonusA, bonusB)
    let hash

    return Object.freeze({
      getEmployeeId: () => employeeId,
      getId: () => id,
      getDepartment: () => department,
      getYear: () => year,
      getModifiedOnDate: () => modifiedOnDate,
      getRemarks: () => sanitizedRemarks,
      getHash: () => hash || (hash = makeHash()),
      getOrderEvaluations: () => validOrderEvaluations,
      getSocialPerformanceEvaluations: () => validSocialPerformanceEvaluations,
      getBonusA: () => bonusA,
      getBonusB: () => bonusB,
      getTotalBonus: () => totalBonus
    })

    function makeHash () {
      return md5(
        employeeId +
        sanitizedRemarks +
        year +
        department
      )
    }

    function validateEvaluations (evaluations, type) {
      switch (type) {
        case 'orderEvaluations':
          return Array.isArray(evaluations) ? evaluations.map((evaluation) => makeOrderEvaluation(evaluation)) : []
        case 'socialPerformanceEvaluations':
          return Array.isArray(evaluations) ? evaluations.map((evaluation) => makeSocialPerformanceEvaluation(evaluation)) : []
      }
    }

    function calculateTotalSubBonus (evaluations) {
      let totalSubBonus = 0
      evaluations.forEach(function (evaluation) {
        totalSubBonus += evaluation.getBonus()
      })
      return totalSubBonus
    }
  }
}
