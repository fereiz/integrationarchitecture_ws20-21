import makeFakeRecord from '../../__test__/fixtures/record'
import makeRecord from './'

describe('record', () => {
  it('must have an employeeId', () => {
    const record = makeFakeRecord({ employeeId: null })
    expect(() => makeRecord(record)).toThrow('Record must have an employeeId')
  })
  it('must have a valid id', () => {
    const record = makeFakeRecord({ id: null })
    expect(() => makeRecord(record)).toThrow('Record must have a valid id')
  })
  it('must have a valid department', () => {
    const record = makeFakeRecord({ department: null })
    expect(() => makeRecord(record)).toThrow(
      'Department must have at least one character.'
    )
  })
  it('can have an id', () => {
    const record = makeFakeRecord({ id: 'invalid' })
    expect(() => makeRecord(record)).toThrow('Record must have a valid id')
    const noId = makeFakeRecord({ id: undefined })
    expect(() => makeRecord(noId)).not.toThrow()
  })
  it('can create an id', () => {
    const noId = makeFakeRecord({ id: undefined })
    const record = makeRecord(noId)
    expect(record.getId()).toBeDefined()
  })
  it('has a valid year or generates it', () => {
    const withYear = makeFakeRecord({ year: 1999 })
    const r = makeRecord(withYear).getYear()
    expect(r).toBe(1999)
    const noYear = makeFakeRecord({ year: undefined })
    expect(noYear.year).not.toBeDefined()
    const d = makeRecord(noYear).getYear()
    expect(d).toBeDefined()
    expect(new Date(d).toUTCString().substring(26)).toBe('GMT')
  })
  it('is modifiedOn now in UTC', () => {
    const noModifiedOnDate = makeFakeRecord({ modifiedOn: undefined })
    expect(noModifiedOnDate.modifiedOn).not.toBeDefined()
    const d = makeRecord(noModifiedOnDate).getModifiedOnDate()
    expect(d).toBeDefined()
    expect(new Date(d).toUTCString().substring(26)).toBe('GMT')
  })
  it('sanitizes its text', () => {
    const sane = makeRecord({
      ...makeFakeRecord({ remarks: '<p>This is fine</p>' })
    })
    const insane = makeRecord({
      ...makeFakeRecord({
        remarks: '<script>This is not so fine</script><p>but this is ok</p>'
      })
    })

    expect(sane.getRemarks()).toBe('<p>This is fine</p>')
    expect(insane.getRemarks()).toBe('<p>but this is ok</p>')
  })
  it('includes a hash', () => {
    const fakeRecord = {
      employeeId: 42,
      remarks: 'Good job, keep it up!',
      year: 1984,
      department: 'sales'
    }
    expect(makeRecord(fakeRecord).getHash()).toBe(
      'efa9f39d78a1a9d7483f6529d7b1451a'
    )
  })
  it('can have order evaluations', () => {
    const record = makeFakeRecord({ orderEvaluations: 'invalid' })
    expect(() => makeRecord(record)).toThrow('Record must have valid order evaluations')
    const noOrderEvaluations = makeFakeRecord({ orderEvaluations: undefined })
    expect(() => makeRecord(noOrderEvaluations)).not.toThrow()
  })
  it('has valid order evaluations', () => {
    const invalidRecord = makeFakeRecord({ orderEvaluations: [{ productName: undefined }] })
    expect(() => makeRecord(invalidRecord)).toThrow('Order Evaluation Post needs a product name')
    const validRecord = makeFakeRecord()
    expect(() => makeRecord(validRecord)).not.toThrow()
  })
  it('has evaluations which can return their values', () => {
    const record = makeRecord(makeFakeRecord({
      orderEvaluations: [{
        productName: 'test',
        client: 'test2',
        clientRanking: 'test3',
        items: 123,
        comment: 'test4'
      }],
      socialPerformanceEvaluations: [{
        type: 'test',
        targetValue: 4,
        actualValue: 2,
        comment: 'test2'
      }]
    }))
    expect(record.getOrderEvaluations()[0].getProductName()).toBe('test')
    expect(record.getOrderEvaluations()[0].getClient()).toBe('test2')
    expect(record.getOrderEvaluations()[0].getClientRanking()).toBe('test3')
    expect(record.getOrderEvaluations()[0].getItems()).toBe(123)
    expect(record.getOrderEvaluations()[0].getComment()).toBe('test4')

    expect(record.getSocialPerformanceEvaluations()[0].getType()).toBe('test')
    expect(record.getSocialPerformanceEvaluations()[0].getTargetValue()).toBe(4)
    expect(record.getSocialPerformanceEvaluations()[0].getActualValue()).toBe(2)
    expect(record.getSocialPerformanceEvaluations()[0].getComment()).toBe('test2')
  })
  it('can have social performance evaluations', () => {
    const invalidRecord = makeFakeRecord({ socialPerformanceEvaluations: 'invalid' })
    expect(() => makeRecord(invalidRecord)).toThrow('Record must have valid social performance evaluations')
    const noSocialPerformanceEvaluations = makeFakeRecord({ socialPerformanceEvaluations: undefined })
    expect(() => makeRecord(noSocialPerformanceEvaluations)).not.toThrow()
  })
  it('has valid order evaluations', () => {
    const invalidRecord = makeFakeRecord({ socialPerformanceEvaluations: [{ type: undefined }] })
    expect(() => makeRecord(invalidRecord)).toThrow('Social Performance Evaluation Post needs a type')
    const validRecord = makeFakeRecord()
    expect(() => makeRecord(validRecord)).not.toThrow()
  })
  it('correctly adds boni', () => {
    const record = makeFakeRecord({
      orderEvaluations: [{
        productName: 'hoover roover',
        client: 'Deutsche Telekom AG',
        clientRanking: 5,
        items: 20,
        comment: 'Good job, keep it up!'
      },
      {
        productName: 'hoover roover v2',
        client: 'Vodafon',
        clientRanking: 2,
        items: 5,
        comment: 'Well done.'
      }],
      socialPerformanceEvaluations: [{
        type: 'Type 1',
        targetValue: 4,
        actualValue: 2,
        comment: 'Da geht noch mehr!'
      },
      {
        type: 'Type 2',
        targetValue: 4,
        actualValue: 1,
        comment: 'Darüber müssen wir aber mal sprechen'
      }]
    })
    const validRecord = makeRecord(record)
    expect(validRecord.getBonusA()).toBe(500)
    expect(validRecord.getBonusB()).toBe(20)
    expect(validRecord.getTotalBonus()).toBe(520)
  })
})
