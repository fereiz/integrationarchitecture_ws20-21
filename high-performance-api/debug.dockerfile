FROM node:14

RUN npm set strict-ssl false

WORKDIR /high-performance-api

COPY package.json ./

RUN npm i

COPY . .

EXPOSE 3000

CMD ["npm", "run", "dev"]