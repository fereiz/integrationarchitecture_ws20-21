package ff.IntegrationArchitectures.crudUI.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderEvaluation {
    private String productName;
    private String client;
    private ClientRanking clientRanking;
    private Integer soldItemsCount;
    private double bonus;

}
