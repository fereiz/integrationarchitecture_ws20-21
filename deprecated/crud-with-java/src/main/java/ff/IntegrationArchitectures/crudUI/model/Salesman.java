package ff.IntegrationArchitectures.crudUI.model;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "salesmen")
public class Salesman {
    private Integer id;
    private String firstname;
    private String lastname;
    private List<EvaluationRecord> evaluationRecords;
}
