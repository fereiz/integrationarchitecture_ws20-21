package ff.IntegrationArchitectures.crudUI.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import ff.IntegrationArchitectures.crudUI.model.Salesman;

public interface SalesmanRepository extends PagingAndSortingRepository<Salesman, Integer> {
    
}