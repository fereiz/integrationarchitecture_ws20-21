package ff.IntegrationArchitectures.crudUI.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.apache.commons.collections4.IterableUtils;
import org.springframework.stereotype.Component;

import ff.IntegrationArchitectures.crudUI.EvaluationRecordNotFoundException;
import ff.IntegrationArchitectures.crudUI.ManagePersonal;
import ff.IntegrationArchitectures.crudUI.SalesmanNotFoundException;
import ff.IntegrationArchitectures.crudUI.model.EvaluationRecord;
import ff.IntegrationArchitectures.crudUI.model.Salesman;
import ff.IntegrationArchitectures.crudUI.repository.SalesmanRepository;
import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class PersonalService implements ManagePersonal {

    private final SalesmanRepository salesmanRepository;

    @Override
    public Salesman createSalesman(Salesman salesman) {
        return salesmanRepository.save(salesman);
    }

    @Override
    public Salesman readSalesman(int salesmanId) throws SalesmanNotFoundException {
        Optional<Salesman> salesmanOptional = salesmanRepository.findById(salesmanId);
        if (salesmanOptional.isEmpty())
            throw new SalesmanNotFoundException(salesmanId);
        return salesmanOptional.get();
    }

    @Override
    public List<Salesman> readAllSalesman() {
        List<Salesman> salesmen = IterableUtils.toList(salesmanRepository.findAll());
        return salesmen;
    }

    @Override
    public Salesman updateSalesman(Salesman salesman) {
        if (salesman.getId() == null)
            return createSalesman(salesman);
        return salesmanRepository.save(salesman);
    }

    @Override
    public void deleteSalesman(Salesman record) throws SalesmanNotFoundException {
        Optional<Salesman> salesmanOptional = salesmanRepository.findById(record.getId());
        if (salesmanOptional.isEmpty())
            throw new SalesmanNotFoundException(record.getId());
        salesmanRepository.delete(salesmanOptional.get());

    }

    @Override
    public void addEvaluationRecord(EvaluationRecord record, int salesmanId) throws SalesmanNotFoundException {
        Optional<Salesman> salesmanOptional = salesmanRepository.findById(salesmanId);
        if (salesmanOptional.isEmpty())
            throw new SalesmanNotFoundException(salesmanId);
        salesmanOptional.get().getEvaluationRecords().add(record);
    }

    @Override
    public EvaluationRecord readEvaluationRecord(int salesmanId, int year)
            throws SalesmanNotFoundException, EvaluationRecordNotFoundException {
        Optional<Salesman> salesmanOptional = salesmanRepository.findById(salesmanId);
        if (salesmanOptional.isEmpty())
            throw new SalesmanNotFoundException(salesmanId);

        for (EvaluationRecord record: salesmanOptional.get().getEvaluationRecords())
            if (year == record.getYear())
                return record;
        
        throw new EvaluationRecordNotFoundException(salesmanId, year);
    }

    @Override
    public List<EvaluationRecord> readAllEvaluationRecords(int salesmanId) throws SalesmanNotFoundException {
        Optional<Salesman> salesmanOptional = salesmanRepository.findById(salesmanId);
        if (salesmanOptional.isEmpty())
            throw new SalesmanNotFoundException(salesmanId);
        
        List<EvaluationRecord> records = salesmanOptional.get().getEvaluationRecords();
        if (records == null)
            records = new ArrayList<>();

        return records;
    }

    @Override
    public EvaluationRecord updateEvaluationRecord(EvaluationRecord record, int salesmanId)
            throws SalesmanNotFoundException, EvaluationRecordNotFoundException {
       Optional<Salesman> salesmanOptional = salesmanRepository.findById(salesmanId);
        if (salesmanOptional.isEmpty())
            throw new SalesmanNotFoundException(salesmanId);
            
        for (EvaluationRecord recordFromSalesman : salesmanOptional.get().getEvaluationRecords())
            if (recordFromSalesman.getYear().equals(record.getYear())) {
                recordFromSalesman = record;
                salesmanRepository.save(salesmanOptional.get());
                return recordFromSalesman;
            }

        throw new EvaluationRecordNotFoundException(salesmanId, record.getYear());
    }

    @Override
    public void deleteEvaluationRecord(EvaluationRecord record, int salesmanId)
            throws SalesmanNotFoundException, EvaluationRecordNotFoundException {
        Optional<Salesman> salesmanOptional = salesmanRepository.findById(salesmanId);
        if (salesmanOptional.isEmpty())
            throw new SalesmanNotFoundException(salesmanId);

        Salesman salesman = salesmanOptional.get();
        Iterator<EvaluationRecord> iterator = salesman.getEvaluationRecords().iterator();
        while (iterator.hasNext()) {
            if (iterator.next().getYear().equals(record.getYear())) {
                iterator.remove();
                salesmanRepository.save(salesman);
                return;
            }
        }

        throw new EvaluationRecordNotFoundException(salesmanId, record.getYear());
    }

    @Override
    public List<Salesman> getSalesmenByAttribute(String attribute, String key) {
        // TODO Auto-generated method stub
        return null;
    }

    
}
