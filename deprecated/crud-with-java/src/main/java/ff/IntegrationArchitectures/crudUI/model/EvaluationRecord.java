package ff.IntegrationArchitectures.crudUI.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EvaluationRecord {
    private Integer year;
    private String comments;
    private List<OrderEvaluation> orderEvaluations;
}
