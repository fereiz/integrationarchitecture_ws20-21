package ff.IntegrationArchitectures.crudUI.api;

import java.net.URI;
import java.net.URISyntaxException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import ff.IntegrationArchitectures.crudUI.SalesmanNotFoundException;
import ff.IntegrationArchitectures.crudUI.model.Salesman;
import ff.IntegrationArchitectures.crudUI.service.PersonalService;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class SalesmanAPI {

    static final String URL_SALESMEN = "/salesmen";
    static final String URL_SALESMEN_ID = URL_SALESMEN + "/{id}";

    private final PersonalService personalService;

    @GetMapping(URL_SALESMEN_ID)
    public ResponseEntity<Salesman> getSalesman(@PathVariable Integer id) throws SalesmanNotFoundException {
        return new ResponseEntity<>(personalService.readSalesman(id), HttpStatus.OK);
    }

    @GetMapping(URL_SALESMEN)
    public ResponseEntity<Salesman[]> getAllSalesmen() {
        return new ResponseEntity<>(personalService.readAllSalesman().toArray(new Salesman[0]), HttpStatus.OK);
    }
    
    @PostMapping(value = URL_SALESMEN)
    public ResponseEntity<Salesman> createSalesman(@RequestBody Salesman body) {
        Salesman salesman = personalService.createSalesman(body);
        HttpHeaders headers = new HttpHeaders();
        try {
            headers.setLocation(new URI(URL_SALESMEN_ID.replace("{id}", salesman.getId().toString())));
        } catch (URISyntaxException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(salesman, headers, HttpStatus.CREATED);
    }

    @DeleteMapping(URL_SALESMEN_ID)
    public ResponseEntity<Void> deleteSalesman(@PathVariable Integer id) throws SalesmanNotFoundException {
        Salesman salesman = new Salesman(id, null, null, null);
        personalService.deleteSalesman(salesman);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
