package ff.IntegrationArchitectures.crudUI.api;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import ff.IntegrationArchitectures.crudUI.EvaluationRecordNotFoundException;
import ff.IntegrationArchitectures.crudUI.SalesmanNotFoundException;
import ff.IntegrationArchitectures.crudUI.model.EvaluationRecord;
import ff.IntegrationArchitectures.crudUI.model.Salesman;
import ff.IntegrationArchitectures.crudUI.service.PersonalService;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class EvaluationRecordAPI {
    static final String URL_EVALUATIONS = SalesmanAPI.URL_SALESMEN_ID + "/evaluations";
    static final String URL_EVALUATIONS_YEAR = URL_EVALUATIONS + "/{year}";

    private final PersonalService personalService;

    @GetMapping(URL_EVALUATIONS_YEAR)
    public ResponseEntity<EvaluationRecord> getEvaluationRecord(@PathVariable Integer id, @PathVariable Integer year) {
        ResponseEntity<EvaluationRecord> responseEntity;
        try {
            responseEntity = new ResponseEntity<>(personalService.readEvaluationRecord(id, year), HttpStatus.OK);
        } catch (SalesmanNotFoundException e) {
            responseEntity = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (EvaluationRecordNotFoundException e) {
            responseEntity = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return responseEntity;
    }

    @GetMapping(URL_EVALUATIONS)
    public ResponseEntity<EvaluationRecord[]> getAllEvaluationRecords(@PathVariable Integer id) {
        ResponseEntity<EvaluationRecord[]> responseEntity;
        try {
            responseEntity = new ResponseEntity<>(personalService.readAllEvaluationRecords(id).toArray(new EvaluationRecord[0]), HttpStatus.OK);
        } catch (SalesmanNotFoundException e) {
            responseEntity = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return responseEntity;
    }

    @PostMapping(URL_EVALUATIONS)
    public ResponseEntity<EvaluationRecord> addEvaluationRecord(@PathVariable Integer id, @RequestBody EvaluationRecord record) {
        ResponseEntity<EvaluationRecord> responseEntity;
        try {
            personalService.addEvaluationRecord(record, id);
            responseEntity = new ResponseEntity<>(record, HttpStatus.OK);
        } catch (SalesmanNotFoundException e) {
            responseEntity = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return responseEntity;
    }

    @PutMapping(URL_EVALUATIONS_YEAR)
    public ResponseEntity<EvaluationRecord> updateEvaluationRecord(@PathVariable Integer id, @PathVariable Integer year, @RequestBody EvaluationRecord record) {
        ResponseEntity<EvaluationRecord> responseEntity;
        try {
            personalService.updateEvaluationRecord(record, id);
            responseEntity = new ResponseEntity<>(record, HttpStatus.OK);
        } catch (SalesmanNotFoundException e) {
            responseEntity = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (EvaluationRecordNotFoundException e) {
            responseEntity = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return responseEntity;
    }

    @DeleteMapping(URL_EVALUATIONS_YEAR)
    public ResponseEntity<EvaluationRecord> deleteEvaluationRecord(@PathVariable Integer id, @PathVariable Integer year, @RequestBody EvaluationRecord record) {
        ResponseEntity<EvaluationRecord> responseEntity;
        try {
            personalService.deleteEvaluationRecord(record, id);;
            responseEntity = new ResponseEntity<>(record, HttpStatus.OK);
        } catch (SalesmanNotFoundException e) {
            responseEntity = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (EvaluationRecordNotFoundException e) {
            responseEntity = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return responseEntity;
    }
}