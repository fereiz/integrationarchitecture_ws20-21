package ff.IntegrationArchitectures.crudUI;

public class EvaluationRecordNotFoundException extends Exception {
    public EvaluationRecordNotFoundException(Integer id, Integer year) {
        super(String.format("No evaluation record with ID %d for the year %d found.", id, year));
    }

}
