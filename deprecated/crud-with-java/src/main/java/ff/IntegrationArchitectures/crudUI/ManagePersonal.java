package ff.IntegrationArchitectures.crudUI;

import java.util.List;

import ff.IntegrationArchitectures.crudUI.model.EvaluationRecord;
import ff.IntegrationArchitectures.crudUI.model.Salesman;

public interface ManagePersonal {
    
    Salesman createSalesman(Salesman salesman);

    Salesman readSalesman(int salesmanId) throws SalesmanNotFoundException;

    List<Salesman> readAllSalesman();

    Salesman updateSalesman(Salesman salesman);

    void deleteSalesman(Salesman record) throws SalesmanNotFoundException;


    void addEvaluationRecord(EvaluationRecord record, int salesmanId) throws SalesmanNotFoundException;

    EvaluationRecord readEvaluationRecord(int salesmanId, int year) throws SalesmanNotFoundException, EvaluationRecordNotFoundException;

    List<EvaluationRecord> readAllEvaluationRecords(int salesmanId) throws SalesmanNotFoundException;

    EvaluationRecord updateEvaluationRecord(EvaluationRecord record, int salesmanId) throws SalesmanNotFoundException, EvaluationRecordNotFoundException;

    void deleteEvaluationRecord(EvaluationRecord record, int salesmanId) throws SalesmanNotFoundException, EvaluationRecordNotFoundException;


    List<Salesman> getSalesmenByAttribute(String attribute, String key);
}
