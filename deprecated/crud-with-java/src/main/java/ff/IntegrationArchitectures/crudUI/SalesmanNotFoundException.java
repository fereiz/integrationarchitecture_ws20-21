package ff.IntegrationArchitectures.crudUI;

public class SalesmanNotFoundException extends Exception {
    public SalesmanNotFoundException(Integer id) {
        super(String.format("No salesman with ID %d found.", id));
    }

}
