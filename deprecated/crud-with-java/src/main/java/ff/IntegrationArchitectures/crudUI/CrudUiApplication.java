package ff.IntegrationArchitectures.crudUI;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import ff.IntegrationArchitectures.crudUI.service.PersonalService;

@SpringBootApplication
public class CrudUiApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(CrudUiApplication.class, args);
		PersonalService service = (PersonalService) context.getBean("personalService");
	}

}
