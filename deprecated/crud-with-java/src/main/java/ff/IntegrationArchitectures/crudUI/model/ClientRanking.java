package ff.IntegrationArchitectures.crudUI.model;

public enum ClientRanking {
    EXCELLENT,
    VERY_GOOD,
    GOOD,
    SATISFACTORY,
    FAILURE
}
