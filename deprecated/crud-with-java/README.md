# crud-with-java #
This directory contains a small spring based REST API which connects to a local mongo database and implements all four CRUD operations:
- Create
- Read
- Update
- Delete