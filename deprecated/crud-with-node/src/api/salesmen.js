const express = require('express');
const monk = require('monk');
const Joi = require('@hapi/joi');
Joi.objectId = require('joi-objectid')(Joi);

const mongoDB = monk(process.env.MONGO_URI);
const salesmen = mongoDB.get('salesmen');
const records = mongoDB.get('records');

const orderEvaluationPost = Joi.object({
    productName: Joi.string().trim(),
    client: Joi.string().trim(),
    clientRanking: Joi.string().trim(),
    items: Joi.number().integer(),
    bonus: Joi.number(),
    comment: Joi.string(),
})
const socialPerformanceEvaluationPost = Joi.object({
    type: Joi.string().trim(),
    targetValue: Joi.number().integer().min(1).max(4),
    actualValue: Joi.number().integer().min(1).max(4),
    bonus: Joi.number(),
    comment: Joi.string(),
})
const orderEvaluationSchema = Joi.object({
    orderEvaluationPosts: Joi.array().items(orderEvaluationPost),
    totalBonusForA: Joi.number(),
})
const socialPerformanceEvaluationSchema = Joi.object({
    socialPerformanceEvaluationPosts: Joi.array().items(socialPerformanceEvaluationPost),
    totalBonusForB: Joi.number(),
})
const salesmanSchema = Joi.object({
    firstname: Joi.string().trim().required(),
    lastname: Joi.string().trim().required(),
    orangeHRMID: Joi.number().integer(),
});
const recordsSchema = Joi.object({
    employeeID: Joi.objectId().required(),
    department: Joi.string().trim(),
    year: Joi.number().integer().required(),
    orderEvaluation: Joi.isSchema(orderEvaluationSchema),
    socialPerformanceEvaluation: Joi.isSchema(socialPerformanceEvaluationSchema),
    remarks: Joi.string().trim(),
    totalBonus: Joi.number(),
});

const router = express.Router();

// READ All
router.get('/', async (req, res, next) => {
    try {
        const items = await salesmen.find({});
        res.json(items);
    } catch (error) {
        next(error);
    }
})

// READ One
router.get('/:id', async (req, res, next) => {
    try {
        const { id } = req.params;
        const item = await salesmen.findOne({
            _id: id,
        });
        if (!item) return next();
        return res.json(item);
    } catch (error) {
        next(error);
    }
})

// CREATE One
router.post('/', async (req, res, next) => {
    try {
        const value = await salesmanSchema.validateAsync(req.body);
        const inserted = await salesmen.insert(value);
        res.json(inserted);
    } catch (error) {
        next(error);
    }
})

// UPDATE One
router.put('/:id', async (req, res, next) => {
    try {
        const { id } = req.params;
        const value = await salesmanSchema.validateAsync(req.body);
        const item = await salesmen.findOne({
            _id: id,
        })
        if (!item) return next();
        const updated = await salesmen.update({
            _id: id,
        }, {
            $set: value,
        });
        res.json(value);
    } catch (error) {
        next(error);
    }
})

// DELETE One
router.delete('/:id', async (req, res, next) => {
    try {
        const { id } = req.params;
        await salesmen.remove({ _id: id });
        res.status(200).send("Success");
    } catch (error) {
        next(error);
    }
})


// READ All
router.get('/:id/records', async (req, res, next) => {
    try {
        const { id } = req.params;
        const items = await records.find({
            employeeID: id,
        })
        res.json(items);
    } catch (error) {
        next(error);
    }
})

// READ One
router.get('/:id/records/:year', async (req, res, next) => {
    try {
        const { id, year } = req.params;
        const item = await records.findOne({
            employeeID: id,
            year: +year,
        })
        console.log(item)
        if (!item) return next();
        return res.json(item);
    } catch (error) {
        next(error);
    }
})

// CREATE One Record
router.post('/:id/records', async (req, res, next) => {
    try {
        const value = await recordsSchema.validateAsync(req.body);
        const inserted = await records.insert(value);
        res.json(inserted);
    } catch (error) {
        next(error);
    }
})

// UPDATE One Record
router.put('/:id/records/:year', async (req, res, next) => {
    try {
        const { id, year } = req.params;
        const value = await recordsSchema.validateAsync(req.body);
        const item = await records.findOne({
            employeeID: id,
            year: +year
        })
        if (!item) return next();
        const updated = await records.update(item, { $set: value, });
        res.json(updated);
    } catch (error) {
        next(error);
    }
})

// DELETE One Record
router.delete('/:id/records/:year', async (req, res, next) => {
    try {
        const { id, year } = req.params;
        await records.remove({ 
            employeeID: id,
            year: +year
         });
        res.status(200).send("Success");
    } catch (error) {
        next(error);
    }
})

module.exports = router;



// localhost:5000/api/v1/salesmen/:id/sheets/:year
/*
get all sheets for given salesman
get one sheet for given salesman and year
create sheet for given salesman and year
update sheet for given salesman and year
delete sheet for given salesman and year



*/