const express = require('express');

const salesmen = require('./salesmen');

const router = express.Router();

router.get('/', (req, res) => {
  res.json({
    message: 'API - 👋🌎🌍🌏'
  });
});

router.use('/salesmen', salesmen);

module.exports = router;
