# crud-with-node
The directory ```crud-with-node``` contains a small nodejs, express based REST API which, like the java project, implements the four CRUD operations on a local mongo database. The base project was cloned from a [starter repo](https://github.com/w3cj/express-api-starter). The database connection is implemented with the [monk package](https://www.npmjs.com/package/monk).
