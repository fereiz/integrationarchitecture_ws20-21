### STAGE 1: Build angular###
FROM node:14.5-alpine as builder-angular

#COPY high-performance-ui/package.json high-performance-ui/package-lock.json ./

#RUN npm ci && mkdir /ng-app && mv ./node_modules ./app

WORKDIR /app

COPY /high-performance-ui .

RUN npm ci

RUN npm run build -- --prod --output-path=dist

### STAGE 2: Setup ###
FROM node:14.5-alpine

WORKDIR /app

COPY /high-performance-api/ /app/

RUN npm ci

RUN npm run build && npm run db && cd dist

COPY --from=builder-angular /app/dist /app/public

EXPOSE 3000

CMD ["node", "server.js"]